setwd("/Users/JWT/Documents/Coding/Hyena-r/fisibase-socialnet")
source('egonet.R')
require(lattice)
require(MCMCglmm)
require(stats)
#require(AICcmodavg)
require(bbmle)
require(glmmADMB) ##says it's not available for this version
#require(gplots)
require(sciplot)
require(plyr)
require(dplyr)
require(ggplot2)

####functions####
clean.df <- function(orig.df){
  df <- orig.df
  #df.na <- df[, !(colnames(df) %in% c("ev_centrality"))]
  #df.u<-df[df$greetings_ego_vcount != 0,]
  df.l <- df[df$sex !='u', ]
  #df.l <- na.omit(df.na)
  df.l[,'birthyear'] = format(as.Date(df.l$birthdate), "%Y") #get year from birthdate
  df.l$period_start <- as.Date(df.l$period_start)
  df.l$period_end <- as.Date(df.l$period_end)
  df.l[,'period_length'] <- df.l$period_end - df.l$period_start
  df.l$period_length <- as.numeric(df.l$period_length) 
  #df.l[, 'ARS'] = df.l$rs_days*365
  df.l$birthdate <- as.Date(df.l$birthdate)
  df.l$dengrad <- as.Date(df.l$dengrad)
  df.l[,'agegrad'] <- df.l$dengrad - df.l$birthdate
  df.l$agegrad <- as.numeric(df.l$agegrad)
  df.l$ego <- as.factor(df.l$ego)
  df.l$birthyear <- as.factor(df.l$birthyear)
  df.l$age_at_last_seen <- as.numeric(df.l$age_at_last_seen)
  df.l$mom <- as.factor(df.l$mom)
  #View(grtStats.l)
  df.l$rank_level = rank_categories(df.l$ego_period_rank)
  df.l$rank_level <- as.factor(df.l$rank_level)
  #df.l.r <- rank_categories(df.l)
  return(df.l)
}

clean.sex.df <- function(orig.df, s = "f"){
  df <- clean.df(orig.df)
  df.s <- subset(df, sex == s)
  return(df.s)
}

clean.sexrmu.df <- function(orig.df){
  newdf <- ai.c[ai.c$sex !='u', ]
  return(newdf)
}

clean.long <- function(orig.df, longevity_days){
  newdf <- orig.df[!is.na(orig.df$longevity_days),]
}

qt.fun <- function(x){
  qt(p=.95,df=length(x)-1)*sd(x)/sqrt(length(x)) 
} 

my.ci <- function(x){
  c(mean(x)-qt.fun(x), mean(x)+qt.fun(x))
}  

glm.longevity <- function(pd, formula, df){
  print(pd)
  df.sub <- subset(df, period == pd)
  fit <- glm(formula , family = Gamma(link = 'inverse'), data = df.sub)  
  plot(fit)
  summary(fit)
}


####data####
load("allnetstatsFeb22.Rdata")

data <- clean.df(allNetStats)
data.f <- clean.sex.df(data, "f")
data.f.cd <- subset(data.f, period == 'cd')
data.f.pg <- subset(data.f, period == 'postgrad')
data.f.long <- clean.long(data.f)
data.f.long.cd <- subset(data.f.long, period == 'cd')

####glm.longevity###
##grts
#degree
glm.longevity("cd", age_at_last_seen~greetings_degree + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~greetings_degree + rank_level, data.f.long)
glm.longevity("adult", age_at_last_seen~greetings_degree + rank_level, data.f.long)

#outdegree - shouldn't mean anything
glm.longevity("cd", age_at_last_seen~greetings_outdegree + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~greetings_outdegree + rank_level, data.f.long)
glm.longevity("adult", age_at_last_seen~greetings_outdegree + rank_level, data.f.long)

#indegree - shouldn't mean anything
glm.longevity("cd", age_at_last_seen~greetings_indegree + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~greetings_indegree + rank_level, data.f.long)
glm.longevity("adult", age_at_last_seen~greetings_indegree + rank_level, data.f.long)

#density
glm.longevity("cd", age_at_last_seen~greetings_density + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~greetings_density + rank_level, data.f.long)
glm.longevity("adult", age_at_last_seen~greetings_density + rank_level, data.f.long)

#strength 
glm.longevity("cd", age_at_last_seen~greetings_ego_strength + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~greetings_ego_strength + rank_level, data.f.long)
##significant
glm.longevity("adult", age_at_last_seen~greetings_ego_strength + rank_level, data.f.long)

glm.longevity("cd", age_at_last_seen~greetings_ego_strength + greetings_density + 
                greetings_degree + rank_level, data.f.long)
####nothing significant
glm.longevity("postgrad", age_at_last_seen~greetings_ego_strength + greetings_density + 
                greetings_degree + rank_level, data.f.long)
####nothing significant
glm.longevity("adult", age_at_last_seen~greetings_ego_strength + greetings_density + 
                greetings_degree + rank_level, data.f.long)
####density and degree significant


##LLs
#degree -- only one that matter, the rest are the same, something weird
glm.longevity("cd", age_at_last_seen~ll_degree + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~ll_degree + rank_level, data.f.long)
#significant
glm.longevity("adult", age_at_last_seen~ll_degree + rank_level, data.f.long)

#outdegree
glm.longevity("cd", age_at_last_seen~ll_outdegree + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~ll_outdegree + rank_level, data.f.long)
#significant
glm.longevity("adult", age_at_last_seen~ll_outdegree + rank_level, data.f.long)

#indegree 
glm.longevity("cd", age_at_last_seen~ll_indegree + rank_level, data.f.long)
#nearly sig
glm.longevity("postgrad", age_at_last_seen~ll_indegree + rank_level, data.f.long)
##significant
glm.longevity("adult", age_at_last_seen~ll_indegree + rank_level, data.f.long)

#density
glm.longevity("cd", age_at_last_seen~ll_density + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~ll_density + rank_level, data.f.long)
#significant
glm.longevity("adult", age_at_last_seen~ll_density + rank_level, data.f.long)

#strength
glm.longevity("cd", age_at_last_seen~ll_ego_strength + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~ll_ego_strength + rank_level, data.f.long)
#significant
glm.longevity("adult", age_at_last_seen~ll_ego_strength + rank_level, data.f.long)

glm.longevity("cd", age_at_last_seen~ll_ego_strength + ll_density + ll_degree + 
                rank_level, data.f.long)
####nothing significant
glm.longevity("postgrad", age_at_last_seen~ll_ego_strength + ll_density + ll_degree + 
                rank_level, data.f.long)
####nothing significant
glm.longevity("adult",age_at_last_seen~ll_ego_strength + ll_density + ll_degree + 
                rank_level, data.f.long)
####density and degree significant



##aggs
#degree
glm.longevity("cd", age_at_last_seen~aggressions_degree + rank_level, data.f.long)
##significant
glm.longevity("postgrad", age_at_last_seen~aggressions_degree + rank_level, data.f.long)
##significant
glm.longevity("adult", age_at_last_seen~aggressions_degree + rank_level, data.f.long)

#outdegree
glm.longevity("cd", age_at_last_seen~aggressions_outdegree + rank_level, data.f.long)
##significant
glm.longevity("postgrad", age_at_last_seen~aggressions_outdegree + rank_level, data.f.long)
#significant
glm.longevity("adult", age_at_last_seen~aggressions_outdegree + rank_level, data.f.long)

#indegree
glm.longevity("cd", age_at_last_seen~aggressions_indegree + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~aggressions_indegree + rank_level, data.f.long)
##significant
glm.longevity("adult", age_at_last_seen~aggressions_indegree + rank_level, data.f.long)

#density
glm.longevity("cd", age_at_last_seen~aggressions_density + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~aggressions_density + rank_level, data.f.long)
#significant
glm.longevity("adult", age_at_last_seen~aggressions_density + rank_level, data.f.long)

#strength ###need to get right data first
glm.longevity("cd", age_at_last_seen~aggressions_strength + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~aggressions_strength + rank_level, data.f.long)
glm.longevity("adult", age_at_last_seen~aggressions_strength + rank_level, data.f.long)

glm.longevity("cd", longevity_days~strength + density + vcount + rank_level, grt.f)
####nothing significant
glm.longevity("postgrad", longevity_days~strength + density + vcount + rank_level, grt.f)
####nothing significant
glm.longevity("adult", longevity_days~strength + density + vcount + rank_level, grt.f)
####nothing significant


##AIs
#degree
##significant
glm.longevity("cd", age_at_last_seen~ais_degree + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~ais_degree + rank_level, data.f.long)
##significant
glm.longevity("adult", age_at_last_seen~ais_degree + rank_level, data.f.long)

#outdegree - shouldn't mean anything
glm.longevity("cd", age_at_last_seen~ais_outdegree + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~ais_outdegree + rank_level, data.f.long)
glm.longevity("adult", age_at_last_seen~ais_outdegree + rank_level, data.f.long)

#indegree - shouldn't mean anything
glm.longevity("cd", age_at_last_seen~ais_indegree + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~ais_indegree + rank_level, data.f.long)
glm.longevity("adult", age_at_last_seen~ais_indegree + rank_level, data.f.long)

#density
glm.longevity("cd", age_at_last_seen~ais_density + rank_level, data.f.long)
glm.longevity("postgrad", age_at_last_seen~ais_density + rank_level, data.f.long)
##significant
glm.longevity("adult", age_at_last_seen~ais_density + rank_level, data.f.long)

#strength ###need to get right data first
glm.longevity("cd", longevity_days~strength + rank_level, grt.f)
glm.longevity("postgrad", longevity_days~strength + rank_level, grt.f)
glm.longevity("adult", longevity_days~strength+ rank_level, grt.f)

glm.longevity("cd", longevity_days~strength + density + vcount + rank_level, grt.f)
####nothing significant
glm.longevity("postgrad", longevity_days~strength + density + vcount + rank_level, grt.f)
####nothing significant
glm.longevity("adult", longevity_days~strength + density + vcount + rank_level, grt.f)
####nothing significant



####GLMM analysis####
grt.deg.cd.gam <- glmmadmb(age_at_last_seen ~ greetings_degree + rank_level + (1|birthyear) + (1|mom), 
                           data = na.omit(data.f.long.cd), family ='gamma')
summary(grt.deg.cd.gam)  

