library(plotrix)
par(mar = c(4,5,1,1))
plot(c(1.7,2), c(34.917, 15.849), type = "o", ylim = c(0, 65), xlim = c(1.6, 2.7), , bty = "n", axes = F,
     ylab = "Mean ± CI number of lions counted", xpd = T, xlab = "Clan", cex.lab = 1.5, lty = 2, lwd = 2, pch = 16, cex = 1.5)
points(c(2.2,2.4,2.6), c(46.362, 13.015, 18.747), cex = 1.5, pch = c(5, 2, 1), ylim = c(0, 60), xlim = c(1.6,2.7))
ablineclip(v = 1.7, y1 = 26.798, y2 = 46.385, lwd = 3)
ablineclip(v = 2, y1 = 12.002, y2 = 20.738, lwd = 3)
ablineclip(v = 2.2, y1 = 35.299, y2 = 60.575, lwd = 3)
ablineclip(v = 2.4, y1 = 9.632, y2 = 17.375, lwd = 3)
ablineclip(v = 2.6, y1 = 14.133, y2 = 23.981, lwd = 3)
axis(2, las = 1, cex.axis = 1.5)
mtext(c("TW", "TW", "SN", "SS", "HZ"), at = c(1.7,2, 2.2, 2.4, 2.6), side = 1, cex = 1.5)
mtext(c("2004-2008"), at = c(1.7), line = 1, side = 1, cex = 1)
mtext(c("2009-2013"), at = c(2.3), line = 1, side = 1, cex = 1)
abline(h = 0)
segments(x0 = 2, x1 = 2.2, y0 = -7, xpd = TRUE) 
segments(x0 = 2.4, x1 = 2.6, y0 = -7, xpd = TRUE) 