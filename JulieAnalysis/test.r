# egonet.R

library("RSQLite")
library("igraph")
# library("sna")
DEFAULT_DB = 'db/fisibase_socialnet.sqlite'

source('fisibase-r/fisibase_db.R')
source('fisibase-r/hyena_queries.R')


c = getConnection(DEFAULT_DB)   

# library("plyr")
# parallel processing...
# library(doMC)
# registerDoMC(2)  # use 2 cores...



# is the Hyena ID in the graph set of vertexes
# there is probably a much better way to do this...
# iGraph version
in.graph = function(g,n) {
  return(n %in% V(g)$name)
}

onePeriod <- function(gdf,ego,period){
  # for testing.  Given a behavior data frame, an ego and period, return a a graph
  tempel = gdf[gdf$ego ==ego & gdf$period == period,c("H1","H2","weight")]
  return(tempel)  
}
# used to subset edgelist by ego and period assumes 
one.graph <- function(gdf,ego,period){
  # for testing.  Given a behavior data frame, an ego and period, return a a graph
  tempel = gdf[gdf$ego ==ego & gdf$period == period,c("H1","H2","weight")]
  return(graph.data.frame(tempel))
  
}


# get data from the database
#greetings.df = dbTable("lifeperiods_greetings_el")
#ll.df = dbTable("lifeperiods_liftleg_el")
#aggressions.df = dbTable("lifeperiods_aggressions_el")


gStats <- function(behavstr= "greetings",
                   lifeperiods.df = dbTable("lifeperiods"), 
                   behav.df =  dbTable(paste("lifeperiods", behavstr, "el", sep="_")) )
{
  # add new columns to the list of egos + periods to store graph statistics
  # for each kind of behavior.  this scheme depends on consistent anming of tables in the database
  # and the behavstr is in the name of the table, it's then used to create the graph statistic columns in the ego data frame
  
  # add test that behav.df has columns we need
  
  # remove all NAs to avoid crashes 
  behav.df = behav.df[complete.cases(behav.df),]
  
  # create empty columns in statistic data frame
  lifeperiods.df[,paste(behavstr,"degree",sep="_")] <- NA
  lifeperiods.df[,paste(behavstr,"vcount",sep="_")] <- NA
  lifeperiods.df[,paste(behavstr,"betweenness",sep="_")] <- NA 
  # closeness is a whole matrx...lifeperiods.df[,paste(behavstr,"closeness",sep="_")] <- 0
  
  # loop through every ego+lifeperiod combo, and add graph statistics to the lifeperiods data frame
  # and fill in the columns above
  for (i in 1:nrow(lifeperiods.df)) {
    
    ego.id= as.character(lifeperiods.df[i, "ego"])
    period.str = lifeperiods.df[i,"period"]
    
    # debug
    # print(ego.id)
    # print(period.str)
    # get a graph from the edge lists for just this ego+period
    period_g = one.graph(behav.df, ego.id, lifeperiods.df[i,"period"])
    
    # calculate statistics and store them
    
    if( in.graph(period_g,ego.id)) {
      
      lifeperiods.df[i,paste(behavstr,"degree",sep="_")] = degree(period_g,ego.id)
      lifeperiods.df[i,paste(behavstr,"betweenness",sep="_")] = betweenness(period_g, ego.id)  
      lifeperiods.df[i,paste(behavstr,"vcount",sep="_")] <- vcount(period_g)
      
    }
    
    rm(period_g)
  }
  
  return(lifeperiods.df)
  
}

## EXAMPLE USE OF THE ABOVE FUNCTION
# use like this my.dataframe = demo_gStats()

# WIP
test_aggs = function(){
  aggs.df = dbTable("lifeperiods_aggressions_el")
  lp.df = dbTable("lifeperiods")
  lp.df = subset(lp.df, ego!="bailey")
  for (egoid in lp.df$ego) {
    test.stats = gStats("aggtest", subset(lp.df,ego==egoid),aggs.df)
  }
  
}

demo_gStats <- function(behav = "greetings") {
  print(paste("Demonstration of gStats program for ",behav))
  c = getConnection(DEFAULT_DB)   # you may have to use egonet/egonetdb.sqlite
  if(c == FALSE){
    print("Problem with database connection, stopping")
    return()
  }
  new.df = gStats(behav)
  egos.df = dbTable('egos')
  return(merge(new.df, egos, by='ego'))
}


####
# testing code
test.ego <-function(hyena="adon",period_name ="postwean"){
  # how to use the function above...
  c <- getConnection(DEFAULT_DB)   # you may have to use egonet/egonetdb.sqlite
  greetings.df = dbTable("lifeperiods_greetings_el")
  hyena.graph = one.graph(greetings.df,hyena, period_name)
  plot(hyena.graph)  # plot par title="mrph,ad1"
  return(hyena.graph)
}


