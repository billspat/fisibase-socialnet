#!/usr/bin/env python

import os
import argparse
from snabuilder import SNABuilder


def crazy_db_builder(dbfile):
    """factory method for snb_dbuilder with params specific to this study"""

    periods = [
        {'seq': 1, 'name': 'something', 'desc': '6 months old to 1 year old',
         'start_expr': "date(hyenas.birthdate, '+6 months')",
         'end_expr': "date(hyenas.birthdate, '+1 year')",
         'where': None},
        {'seq': 2, 'name': 'else', 'desc': '1 year old to 18 months',
         'start_expr': "date(hyenas.birthdate, '+1 year')",
         'end_expr': "date(hyenas.birthdate, '+18 months')",
         'where': None},
        {'seq': 3, 'name': '18to24', 'desc': '18 months old to 2 yrs',
         'start_expr': "date(hyenas.birthdate, '+18 months')",
         'end_expr': "date(hyenas.birthdate, '+24 months')",
         'where': None}

    
    # options
    clan = 'talek'
    max_session_date = "2013-09-30"  # current db as of
    clanlist_seen_threshold = 10
    # provide sql for how hyenas will be seleted for this study
    select_hyenas_sql = """SELECT hyenas.hyena as hyena
            FROM hyenas
            WHERE   hyenas.birthdate is not null
                and hyenas.mom       is not null
                and hyenas.firstseen is not null
                and hyenas.dengrad   is not null
                and julianday(hyenas.firstseen) < julianday(date(hyenas.birthdate, "+90 days"))
                and strftime('%Y', hyenas.firstseen) >= '1989' and hyenas.clan='{0}'
                and julianday(hyenas.firstseen) < julianday('{1}')
            order by  hyenas.birthdate;
            """.format(clan, max_session_date)
            
    crazybuilder = SNABuilder(dbfile, periods, max_session_date, select_hyenas_sql, clanlist_seen_threshold)
    crazybuilder.periods[4] =  {'seq': 4, 'name': '18to24', 'desc': '18 months old to 2 yrs',
         'start_expr': "date(hyenas.birthdate, '+18 months')",
         'end_expr': "date(hyenas.birthdate, '+24 months')",
         'where': None} 
    crazybuilder.clan = 'South'
    crazybuilder.builders = [SessionsPlusMaker(self.db, self.max_session_date),
                 EgonamesMaker(self.db, self.max_session_date, self.select_hyenas_sql, self.clan),
                 EgosTableMaker(self.db, self.max_session_date),
                 PeriodsTableMaker(self.db, self.periods),
                 GCLifePeriodsTableMaker(self.db, self.periods, self.max_session_date),
                 ClanListsMaker(self.db, self.clanlist_seen_threshold),
                 AIPairsMaker(self.db),
                 AItogethersessionsMaker(self.db),
                 AisElMaker(self.db),
                 GreetingsElMaker(self.db),
                 LiftlegbothElMaker(self.db),
                 LiftlegElMaker(self.db),
                 AggressionsElMaker(self.db),
                 DyadicAggressionsElMaker(self.db)
                ]
    result = crazybuilder.build_db()   



def valid_file(filepath):
    if not os.path.exists(filepath):
        msg = "can't find database file %s" % filepath
        raise argparse.ArgumentTypeError(msg)
    return filepath


if __name__ == "__main__":
    """ send a valid file name for a db, one that has the required tables and no other SNA tables"""
    parser = argparse.ArgumentParser()

    parser.add_argument('dbfile',
                        help='initial database with fisibase tables (required)',
                        type=valid_file ) 

    parser.add_argument('--verbose', '-v', required=False, help='print progress on command line',
                        action="store_true")
    myargs = parser.parse_args()

    if myargs.verbose:
         print('building sna tables in {0}'.format(myargs.dbfile))
    

    if myargs.verbose:
        builder.print_result()


    