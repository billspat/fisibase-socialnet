Hyena database for social networking analysis: fisibase-socialnet ===

Overview
---

These collection of programs enable analysis of demographic and behavior event tables in the hyena database for social networking.  It does so by creating edgelists of graphs to be used by the SNA R package.

**Goals**
The goal with this system is for graduate students to tweak those builder programs for current data or changes in parameters (adjust dates, alter how hyenas are selected, etc).  Python/SQL programmers can write new study programs to make new studies, or add new table builders and incorporate these.  

The hyena database has demographic (hyenas table), ecological (prey, rainfall), behavioral (aggressions, appeasements, greetings, other?) and sessions (situational?)

SNA analysis creates 'graphs' with hyenas as vertices (nodes) and their relationship as edges (links) and added facets or attributes to vertices and edges based on other information.  Ego-nets are graphs centered around a particular individual and include only those vertices with edges to that ego vertex.    These graphs are made for particular time periods and clan.  Because they are ego neds, the clan and time period are based on individuals hyenas and a period of that hyena (e.g. from 6 months old to 12 months for hyena "Helios" in clan "Talek West").   

An edgelist of a graph needs two tables: table vertices (Hyenas and their attributes for the time period), table of pairs of vertices indicating they are connected, and attributes of those connections.   

An example is a graph of aggressions for talek west clan during the month of July, year 2000
vertex table : list of clan members during that time period, their ages, sex, rank, etc.  note there are several methods for determining clan membership for a time period, not simply who was present in the sessions at that time.   
edge table: for all agressions, list the agressor as the first animal and recipient as the second.  Give the edge the weight as the intensity of the agression.   Sum this list so each pair appears just once, then sum the intensities for the weights.   Potentially adjust the weights by some other factor, such as the association index of the pair for that time period.  And any other refinements

**To build these graphs, The workflow is**

  * copy the latest data into this project ( build a SQLite version of the database with the program in https://gitlab.msu.edu/billspat/fisibase-sqlite ) 
     * note this program will add tables to the database, so use a copy of it if you want to keep a non-sna version
  * read existing database tables from that sqlite file
  * select the 'egos' (individuals) to be used in the study from hyenas table using the criteria for your sna study (more on that below)
  * add or update data from outside database if any (current don't need this)
  * create tables the time periods in question, either by calendar, or based on ego animals developmental periods (e.g. by age range, or developmental milestone).   
  * With time periods set, calculate new items to demographic tables if needed for each time period.   For example, how many sessions were animals seen during that time period.   What were the animals' (average) ranks during that time period (or assign mothers' ranks, etc).
  * Determine clan list members during each time period by some criteria, include animals seen during that time period for some threhsold number of sessions (eg n>10 sessions).   Alternatively select animals in more than some threshold of session for a larger time period (e.g. during that year)
  * given list of time periods, and clan lists, build edgelists for each graph needed by reading behavior tables (or table sessions to calculate AIs) and including clan members included in those tables based on some criteria for those behaviors.
  * the first of these edgelists is the AI table so that behaviors are controlled for opportunities (count of behaviors divided by AI to give edge weights). 


Then these edgelist tables are imported into the graph analysis program ( in R ) and graph stats calculated on them.   


Scripts
---

For our study I use this combination of code

**convert to sqlite** : using (mac/linux) shell script and program called mdbtools converted MS access to sqlite
  * A copy of the MS Access is updated with Sarah's rank, and updated aggressions data
  * I use sqlite as one can program more robust sq commands.   
  * Once converted to sqlite, the tables are adjusted for compatibility and naming is simplified, and indexes are added (SQL indexes speed up queries dramatically)
  * code is kept in a seperate project https://gitlab.msu.edu/billspat/fisibase-sqlite  

**import** using shell script import prey data into the sqlite (note: this was for prey data only, but now prey density table is in MS Access so no necessary)

**Create new tables for SNA with SQL** 

To create the new tables of egos, periods, edgelists, etc, we have SQL statements that read from existing tables, and create new tables

  * this must be done in a certain order as some tables depend on others
  * this must be tailored to the particular SNA study, e.g. for different time periods and animals to include the resulting edge lists are different, and thresholds for creating clans lists, and other parameters are different.  
  * SQL by itself does not have code to organize this, so the framework is written in Python, and python generates the SQL statements, which are sent to SQLite to build a sqlite database.    
  * Python is  

**analyze the SNA database** The graph and SNA analysis is done in R, where the


** notes about process **

so in short, what you do is 1) get an updated sqlite database with the process we went over before, 2) run the python program to build the socialnetwork database for your study and 3) stick that into R and do the analysis.   
to build a database, (step 2 above), you either a) take a working “dbbuilder” python program I wrote that sets the parameters for the ‘study’ (the periods, specs for which egos to pick, etc) and run it, or b) make a new builder program with new parameters.   I tthink that perhaps a good way to go through it is to first go through those 3 steps for something I get working, then go through them but create the Python db builder program for the twins study together.

**Description of SNA builder programs**


Builder programs set values of variables for your study (which intervals or periods of life history are of interest, criteria for selecting hyenas as egos, etc), uses those to create special 'builder' objects for each of the tables needed (aggressions edge lists for example).  These study builders control how and which tables are added to the database.   The database builder program send parameters to the table builders, and launch them in the necessary order.   
Becuase this requires control, the builder program is python, and it constructs the string of SQL and sends that to the database. 

First, there is a program for connecting to a database in Python: **sqlitedb.py**
Then creating a table with SQL has common steps  (create table, insert data, add additional data to it, create indexes, etc) so that's abstracted in **tablemaker.py**  which is a class so one creates tablemaker objects with the specifics e.g. EgosTableMaker <- TableMaker
You actually create the table by setting up all the SQL in EgosTableMaker, then running function EgosTableMaker.build()   You also set up dependencies - tables that need to be made first - so when build() is run it checks to make sure those are present. 

All of code that creates builder objects for tables are in **tablebuilders.py**  These builder objects take parameters for your study.  For example  clanlists need to know minimum number of times animal must be seen to be part of clan, so we have   


    db = sqlitedb.SqliteDB('myfisibasecopy.sqlite')
    clanlist_seen_threshold = 10
    tablebuilder = ClanListsMaker(self.db, clanlist_seen_threshold)
    tablebuilder.build()

I use what are called 'singleton classes' like "ClanListsMaker" (classes you create just once in your big program) so that the "setting parameters" step can be seperated from the "build" step for flexibility, allowing you to add more steps in between (like filtering out data rows from other tables, etc.  This won't work unless the tables that ClanListsMaker has set as dependencies are also present.  

Sometimes not everyting can fit in a single SQL insert records statement when creating a table.  So _tablemaker.py_  has a step called "_bonus_sql()" where any extra sql or other steps can be run before the table is declared finished.  Many of the table builders, especially _EgosTableMaker_, use this bonus sql step to run specially sql calculations, like reproductive success, and insert that special data into a table column.  This is not elegant but it works for now. 

The program that controls all of these by setting parameters and running .builder() for all the tables in order is **snabuilder.py**   It sets variables, tableMaker objects for each table (in an array), and runs all the builders (by loop through the array).  Because building an SNA database for any kind of study has common steps and common variables/parameters, this is a python class which can be used as a bases for specific study SNA database.   

Study builders are **milestone\_snabuilder.py**, **maledispersal\_snabuilder.py**  etc.  These programs have command line argument parse stuff so you can simply give it a database name from the terminal to run it.   Because _snabuilder.py_  program covers most of the work, these programs simply set study-specific variables, and use the snabuilder steps to build.  You could however add new, study specific tables in these classes

You can then build a database by first preparing a sqlite database from the latest AccessFisi (with Ranks added), say **mydb.sqlite**  Then run these commands in the terminal (# are comments and not needed) (Windows users must install Python 2 and libraries; Anaconda is best way as you get all needed libraries like pysqlite)

    # GET INTO THE DB FOLDER
    cd <somewhere>/fisibase-socialnet/db
    # COPY SQLITE DATABASE INTO THIS FOLDER
    cp <where my sqlite db is>/mydb.sqlite .
    # RUN PYTHON PROGRAM
    python maledispersal_snabuilder.py test-maledispersal.sqlite
 
This will take a long time, depending the size of the egos table and length of periods.  This is only because the AIs table, the other tables are quick, but the AIs
table is needed before creating the other edge lists.    ( ~ 1-2 hrs on fast laptop).   While it's running, if you close your computer (put to sleep) there is a chance there will 
be a file error. 

TODO: describe how to use redirection to capture log of sql code  

    python maledispersal_snabuilder.py test-maledispersal.sqlite > maledispersal_sqlcommands.sqlite
    
    
**About Python Classes/Objects**

Python classes are used throughout, instead of lots of function, to help organize related code.  This can be confusing as when you make a 'subclass'  like 

    dbbuilder = SNABuilder(dbfile, periods, max_session_date, select_hyenas_sql, clanlist_seen_threshold,  myargs.verbose)
    dbbuilder.build()
    
Many of the details seem 'hidden' so need to look into the snabilder.py program.  Also there are sevearl ways to set or override variables in classes, and sometimes use different ways as this is untested project.    Please look over great websites like https://jeffknupp.com/blog/2014/06/18/improve-your-python-python-classes-and-object-oriented-programming/  to learn about them.   Notes that Python 2 and Python 3 have different features re classes/objects.  This code uses Python 2.   

