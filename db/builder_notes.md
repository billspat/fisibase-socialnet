SNA database builder notes
===

background
----

To study SNA, need to see social relationships of an individual during different parts of an individuals developement.   Social rels are examined via  build collections of edge lists for each individual of interest, and those edge lists a

goal
----

to test different hypotheses related to social development, build different groups of hyenas and life periods/milestones and compare how those change over time, or compare the periods across individuals based on other characteristics such as rank, etc

__current use cases__

milestones: females and males use specific milestones and build up periods between those (dengrad to dispersal) and compare across individuals for rank. Select only those animals who have n > 10 sessions (obs) in each milestone period.  Period length is inter-milestone length

milestone/consistent size: same criteria above, but periods are set at 6 months length starting at each milestone

dispersal: males only, create periods of 1 month back dating from date of dispersal.  Include all natal males (known birthdate) seen at least 2? times per month.  For every month?  

need
---
way to build a specific SNA database that has collections of edge lists
means to specify 

  # the list of animals to be used
    by specifying the criteria for selecting animals 
  # the characteristics of the periods
  # which edge lists will be included in the resulting SNA database
  
is  building sna database for specific use cases or specific analysis


implementation 
---

The program builds a new Sqlite database every time.   That is, results of each function creates new table(s) that are used in subsequent functions.  In the future it may be better to add/alter tables as needed using dependencies. 

Outline of code base to meet needs above. 

open database from comman line and start processing

function that can include animals from database(s).  Input will be a  specification of criteria for including animals.  start with  sql selection code that returns known columns but would be good to make it generic so that multiple databases can be used.  could includ just the where clause which still assume certain structure.  Can subselects be included in where clause?   will be hard to make this generic

characteristics of the periods.  This is mostly done, just need to accept an array of specs

minimum session count per period (threshold for inclusion)

list of edgelist functions to run, or all of them.   If "all" of them, master program needs to keep a list of all edges lists
to include any new edgelists without explicitly mentioning them.  Start with just a list of programs.   specialized edgelist code may be needed for any one case

all cases require the association index edge lists.  These are used to 'normalize' the weigths of other edge lists, which are simply counts of behaviors.  AIs SHOULD be calculated first but these take a long time to run, so often are not. 



ai_normalizer(edgelist,aitable) run after both an edgelist and ai edge lists is run

some tables require additional sql to run to fill columns.  Perhaps thes should run as dependencies?  For example session counts, "age_last_seen_table_sql" used to fill in age_last_seen.   process is 1) build table with base columns, 2) add new column and fill it using sql, 3) repeat 2 for each column needed.   This way each analysis can have custom columns added to base tables

sqlite supports statement to add columns but not remove, e.g. "alter table X add column NewColumn coltype;"   Note no parenthesis are used unlike the "create table..." statement.  

