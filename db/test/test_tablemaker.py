"""test of table maker object"""
from .. import sqlitedb # import SqliteDB
# from .. import tablemaker # import TableMaker
from shutil import copyfile
from .. import dbbuilder #  import SessionsPlusMaker,EgonamesMaker
import os
print(os.curdir)
os.chdir('..')

startdbfile = 'fisibase.sqlite'
workingdbfile = 'test.sqlite'
copyfile(startdbfile,workingdbfile)

db = sqlitedb.SqliteDB(workingdbfile)
tablename = "egonames"
clan = 'talek'
max_session_date = "2013-03-30"
select_hyenas_sql = """SELECT hyenas.hyena as hyena
    FROM hyenas
    WHERE   hyenas.birthdate is not null
        and hyenas.mom       is not null
        and hyenas.firstseen is not null
        and hyenas.dengrad   is not null
        and julianday(hyenas.firstseen) < julianday(date(hyenas.birthdate, "+90 days"))
        and strftime('%Y', hyenas.firstseen) >= '1989' and hyenas.clan='{0}'
        and julianday(hyenas.firstseen) < julianday('{1}')
    order by  hyenas.birthdate;
    """.format(clan, max_session_date)

# testmaker = TableMaker(db,required_tables=['hyenas'], tablename = tablename, table_sql = "", verbose=True)
#
# testmaker.table_sql = """CREATE TABLE egonames (hyena VARCHAR, PRIMARY KEY(hyena));"""
#
# testmaker.insert_sql = "INSERT INTO egonames (hyena) {0}".format(select_hyenas_sql)
#
# result = testmaker.make_table()
#
# print("{0} was made = {1} ; and db has table: ".format(testmaker.tablename, result))
# print(db.has_table(tablename))


builders = [dbbuilder.SessionsPlusMaker(db, max_session_date), dbbuilder.EgonamesMaker(db,max_session_date, clan="talek")]

for builder in builders:
    # run tablemaker code... and test that it was made
    print("making {0}")
    result = builder.make_table()
    print("{0} was made = {1} ; and db has table: ".format(builder.tablename, result))
    print(db.has_table(builder.tablename))
