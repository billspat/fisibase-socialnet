# UNTESTED PSUEDOCODE, kind of python
#  hyenarelatedness 
# movtivated by this problem: assign a lineage ID to all hyenas if they have one.  Then assign relatedness values using that information 
# so very few generic methods, only those needed to build lineages
# Using a generic "hyenas" class and not Clan class as  clans split and matriarchie's couldn't be traced
# this is saved as part of this project inorder to preserve the ideas here

import sys, os
 
from sqlitedb import SqliteDB, sqlite_versions

# nano-ORM for sqlite.  
class FbTable():
    def __init__(self, db, table_name, id_field="ID"):
        self.db = db
        self.table_name = table_name
        self.id_field = id_field
        # check if db has this table, if not throw error
        self.records = self.read_data()  
        
    def find_all(self):
        return(self.db.table_data())
    
    def find(id_value):
        
        
    def all(self):
        return(self.records)


class Hyenas(FbTable):
     """list of hyenas keyed on hyena id"""
    # could probably inherit from dict or something
     def __init__(self, db):
          super().__init__(hyenas, db)
          self.hyenas = self.data 
          self.import_data(database,tablename)

     def import_data(database,tablename):
          #  connect to sqlite3 db
          #  read the table as table name
          #  select * from 'tablename'
          # create a list of Hyena objects

          hyenas_table = []  # get something from the databae table
          for h_record in hyenas_table:
                h_id = # Get the ID from h_record
                    self.hyenas[h_id] = Hyena(h_record)

     def build_lineages(self):
         # to hyenas table, add columns 
             # has_offspring t/f, and 
             # matriarch_id : char(10) self join on hyenas table
             
          self.identify_moms()  # first ID the moms
          for hyena in self.hyenas:
               assign_matriarch(hyena)     

     def identify_moms(self):
          """ assuming there is no "has offspring" field, as there is now, 
          add new field to all hyenas and populate with a simple loop
          """
          for hyena in self.hyenas:
               if(hyena.mom_id):
                    mom = self.hyenas[mom_id]
                    # hyenas class must have a 'has_offsrping' field
                    mom.set_has_offspring(True)

     def assign_matriarch(hyena): 
          """recursive function to look for and assign matriarch_id to all hyenas"""
          if (hyena.matriarch_id):
               # has a lineage assigned already, stop here

         elif (hyena.mom_id):
              # has known mother, search up tree with recursion
              mom_hyena = hyenas[hyena.mom]
              hyena.matriarch_id = assign_matriarch(mom_hyena)

         # no mom if we get here
         elif (hyena.has_offspring):   # see function above needed to pre-assign this field value
               # hyena is a matriarch herself
               hyena.matriarch_id = hyena.hyena_id

          else: 
               # singleton, immigrant, or little known clan: no lineage
               hyena.matriarch_id = None

          # endif
         return(hyena.matriarch_id)  # always return the matriarch_id, for the calling function

    def relatedness(h1,h2):

        # first filter unrelated
        if( h1.matriarch != h2.matriarch): return(0)

        # offspring: mom and child are 0.5
           if (h1.mom == h2 or h2.mom == h1):
               return(0.5)
        # siblings:   same mom and same birthdate r = 0.5
           if (h1.mom == h2.mom and h1.birthdate == h2.birthdate):
               return( 0.5)
        # 1/2 sibs:   same mom  but not same litter r = 0.25
           if (h1.mom == h2.mom and h1.birthdate != h2.birthdate):
              return(0.25)
        # ancestors (female,  grandmothers; great, etc)  r=0.25
           # related but not offspring or sibs if we've gotten this far
           # which h1 h2 is older?
          if h1.birthdate > h2.birthdate :
              elder = h1
              younger = h2
          else
              elder = h2
              younger = h1

          ancestor = younger.mom

          while ancestor.mom != null
              ancestor = ancestor.mom
              if (ancestor == elder): return(0.25) # found ancestor
          if dropped out of loop then not an ancestor

        #  aunts  r = 0.25 ; NOT great aunts or cousins
          #  if H1 in set of H2's mom's sisters?
              # => aunt, return (0.124)
          #  if H2 in set of H1's mom's sisters?
              
        # because we've done the work of finding the common matriarch
        #  and these are related but not direct ancestors
        # check if great aunt of cousins
        #  need way to find that?
         return(0.125)


class Hyena():
     """record of hyena from dtTable - should find an existing db record class to use to inherit from"""
     def __init__(self, hyena_id, db, dbtable="hyenas"):     
          self.hyenas = hyenas  # list membership
          self.hyena_id = hyena_id,
          self.db = db
          self.matriarch_id = None
          self.has_offspring = False
          self.hyena_data = None
          self.load_data() 
    
    # should use a db table class      
    def load_data(self):
        qry = "select * from {0} where hyena_id = {1}".format(self.dbtable, self.hyena_id)
        result = db.execsql(qry)
        self.hyena_data = result[0]
        return(self.hyena_data)
        
    def set_has_offspring(self,v):
        self.has_offspring = v
        
    # def findDenGrad(self):
    #     qry = """SELECT session, location, sessiondate, start
    #             FROM tblSessions
    #             WHERE (hyenas LIKE '%,{0},%')
    #             ORDER BY DATE, START""".format(self.hyena_id)
    #
    #     AwayDenSessionCount = 0
    #     AwayDenSessionCountThreshold = 3
    #     DenGrad = None
    #     # check make sure this animal in database, and is a natal animal
    #     db.execute(qry)
    #
    #     session = db.fetchone()
    #     while session and intNonDenSessionCount < SessionCountThreshold
    #         if session["location"].upper() in "ND":
    #             intNonDenSessionCount = 0
    #             DenGrad = None
    #         else:
    #             # not in den, add to count
    #             AwayDenSessionCount += 1
    #             # first away session is the den grad, so lets save it
    #             If AwayDenSessionCount == 1:
    #                 DenGrad = session["sessiondate"]
    #
    #     return DenGrad
        
        
        

if __name__ == '__main__':

    if len(sys.argv) < 3:
        sys.exit('Usage: %s database-name table-name' % sys.argv[0])
    
    if not os.path.exists(sys.argv[1]):
        sys.exit('ERROR: Database %s was not found!' % sys.argv[1])
                
    database_file  = argv[1]
    dbtable = argv[2]
    hyenas = Hyenas(databse,dbtable)

