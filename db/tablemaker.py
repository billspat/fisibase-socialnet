# tablemaker.py : class to create new tables given collection of sql code for each part
#
class TableMaker(object):
    """base class framework for making new tables with sql.  Checks a list of dependent tables if any"""

    def __init__(self, db, required_tables=[], tablename = "TableName", table_sql = "", verbose=False):
        self.required_tables = required_tables
        self.db = db
        # check db is valid?
        self.tablename =  tablename
        self.table_sql = table_sql if table_sql else 'create table {0} (id varchar PRIMARY KEY, data varchar);'.format(self.tablename)
        self.insert_sql = ""
        self.index_sql = []
        self.bonus_sql_str = ""
        self.verbose = verbose

    # methods to emit SQL, override these for table

    def check_dependencies(self):
        """check that tables in dependencies array are present in db.
        """
        for t in self.required_tables:
            if not self.db.has_table(t):
                RuntimeError("To create {0}, required table {1} no in the database".format(self.tablename,t))

        return (True)

    def make_table_sql(self):
        """SQL specific to this table; override this method or replace self.create_sql """
        return (self.table_sql)

    def insert_records_sql(self):
        """SQL code to insert record"""
        # TODO: rename to verb insert_records()
        return (self.insert_sql)

    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        # TODO: rename to verb create_indexes()
        return (self.index_sql)

    def bonus_sql(self):
        """ optional extra sql post create """
        # TODO: rename to verb run_bonus_sql()
        return (self.bonus_sql_str)

    # generic methods to exec sql and test
    def make_table(self):
        """ makes the table with class-specific sql"""

        # first, check dependencies, other tables exist?
        self.check_dependencies()
        print(self.db.tables())
        
        self.db.drop_table(self.tablename)
        if(self.verbose):
            print(self.__str__())

        self.db.execddl(self.make_table_sql())

        # this above command may insert the data, too
        if self.insert_records_sql():
            self.db.execddl(self.insert_records_sql())

        if self.bonus_sql():
            self.db.execddl(self.bonus_sql())

        if self.table_empty():
            # TODO: is empty table always an error condition?
            raise RuntimeError("table {0} has no data".format(self.tablename))

        if self.index_sql_array():
            """ if there is sql code to make indexes, do that"""
            for sql in self.index_sql_array():
                self.db.execddl(sql)

        return (self.db.has_table(self.tablename))

    def __str__(self):
        """string of all sql statements here"""
        sqlstr = self.make_table_sql()


        if self.insert_records_sql():
            sqlstr += "\n" + self.insert_records_sql()

        if self.bonus_sql():
            sqlstr += "\n" + self.bonus_sql()

        if self.index_sql_array():
            for indexsql in self.index_sql_array():
                sqlstr += "\n" + indexsql

        return(sqlstr)


    def table_empty(self):
        """test table data"""
        return not self.db.table_has_data(self.tablename)
