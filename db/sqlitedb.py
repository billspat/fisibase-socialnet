"""class and support functions to open and work on a sqlite3 db file via python """
import os
import sys
# If latest pysqlite2 is on the system use that, but fall back to standard
# sqlite3

try:
    from pysqlite2 import dbapi2 as sqlite3
except ImportError:
    import sqlite3


def sqlite_versions():
    """gets sqlite system information as an array """
    return((sqlite3.version, sqlite3.sqlite_version, sys.version))


def newer_sqlite_version(minversion='3.8.0'):
    """T/F that the sqlite version new enough.  default to 3.8 is simply the version we needed at this time.  """
    import re

    # compare version numbers using cmp, return 1 or -1
    # http://stackoverflow.com/questions/1714027/version-number-comparison
    def versioncmp(version1, version2):
        def normalize(v):
            return [int(x) for x in re.sub(r'(\.0+)*$', '', v).split(".")]
        return cmp(normalize(version1), normalize(version2))

    return (versioncmp(sqlite_versions()[1], minversion) >= 0)


def new_db(dbfile):
    """ creates new 'database file' which for sqlite can be an empty file"""
    conn = sqlite3.connect(dbfile)
    cursor = self.conn.cursor()  # not sure we need this
    conn.close()
    return(dbfile)

###############


class SqliteDB():
    """very simple class to simplify db ops and maintain connection"""

    def __init__(self, dbfile, journal_mode="OFF"):
        if not os.path.exists(dbfile):
            raise RuntimeError("can't find file " + dbfile +
                               ". Please use an existing sqlite database file.")

        self.dbfile = dbfile
        self.journal_mode = journal_mode
        # TODO: add try/catch block here
        self.conn = sqlite3.connect(dbfile)
        self.cursor = 0  # place holder for cursor
        self.current_sql = ""
        self.DEBUG = True
        # TODO check the journal_mode param for valid options and set
        # DELETE | TRUNCATE | PERSIST | MEMORY | WAL | OFF
        if (self.journal_mode == "OFF"):
            self.set_cursor("PRAGMA journal_mode = OFF;", execute=True)

    def set_cursor(self, sql, execute=False):
        """db connections require a cusor object for sql commands, this sets
        and optionally executes the sql """
        self.cursor = self.conn.cursor()
        self.current_sql = sql
        if (self.DEBUG):
            print (self.current_sql)

        if execute:
            self.cursor.execute("pragma synchronous=off")
            self.cursor.execute(sql)

        return(self.cursor)

    def execsql(self, sql, verbose=False):
        """executes a single SQL statement and returns all rows selected.   """
        # TODO rename this since it is used only for SELECT
        if(verbose):
            print(sql)

        self.set_cursor(sql).execute(sql)
        return(self.cursor.fetchall())

    def execddl(self, sql, verbose=False):
        """executes sql statement(s) that are not expected to return row sets"""
        if(verbose):
            print(sql)

        self.set_cursor(sql)

        # self.cursor.execute("PRAGMA journal_mode = OFF;")
        result = self.cursor.executescript(sql)
        # self.cursor.execute("PRAGMA journal_mode = ON;")
        return(result)

    def table_data(self, tablename):
        """get all rows from a single table"""
        tablesql = "select * from {0};".format(tablename)
        return(self.execsql(tablesql))

    def table_rowcount(self, tablename):
        """ get number of rows in a table"""
        # if (! self.has_table(tablename)):
        #    return None

        sql = 'select count(*) from {0}'.format(tablename)
        result = self.execsql(sql)  # fetches all rows
        n = result[0][0]
        return(n)

    def has_table(self, tablename):
        """ T/F table is in the database"""
        # TODO: USE tables function and pull from resulting array
        sql = """SELECT count(name)
               FROM sqlite_master
               WHERE type='table'
               AND name='{0}';""".format(tablename)
        cur = self.conn.cursor()
        # try
        cur.execute(sql)
        n = cur.fetchone()[0]
        return n > 0

    def table_has_data(self, tablename):
        """ quick check at that a table has data in it"""

        n = self.table_rowcount(tablename)
        return n > 0

    def drop_table(self, tablename, verbose=False):
        """standard drop table.  execddl turns on and off journal_mode """
        dropsql = "DROP TABLE IF EXISTS '{0}';".format(tablename)
        result = self.execddl(dropsql, verbose)
        return(result)

    def tables(self):
        """get list of tables from master  db catalog"""
        tablesql = "select name from sqlite_master where type = 'table';"
        return(self.execsql(tablesql))

    def close(self):
        self.conn.commit()
        self.conn.close()
