"""SNABuilder is a base class for creating an SNA database from fisibase with given intial parameters.
  It includes parameters to stand on its own, but you should create your own instance or class
  and override them for your SNA study.  The reason for this complexity is so one can vary input parameters but
  still maintain most of the existing SQL to build edge list tables

  Usage
  # first copy fisibase.sqlite to snastudy.sqlite or something
  # then setup the parameters you need for the study
  # including the periods
   >>>studyperiods = [
        {'seq': 1, 'name': '6to12', 'desc': '6 months old to 1 year old',
         'start_expr': "date(hyenas.birthdate, '+6 months')",
         'end_expr': "date(hyenas.birthdate, '+1 year')",
         'where': None},
        {'seq': 2, 'name': '12to18', 'desc': '1 year old to 18 months',
         'start_expr': "date(hyenas.birthdate, '+1 year')",
         'end_expr': "date(hyenas.birthdate, '+18 months')"} ]

  # create a builder object, database file and periods are required parameters
   >>>snastudybuilder = SNABuilder('snastudy.sqlite', periods=studyperiods)
  # set other optional parameters
   >>>snastudybuilder.clan = 'talek'
   >>>snastudybuilder.max_session_date = "2013-03-30"  # current db as of
   >>>snastudybuilder.clanlist_seen_threshold = 10
   # run the builder that adds tables to the sna sqlite db
   >>>sna.build_db()
   # the
"""

from   tablebuilders import *
import sqlitedb


class SNABuilder():
    """base class for builders of SNA databases.  Given parameters, creates db tables by using
    builder objects for each table listed in builder.builders array, in order.
    Each study either instantiates with periods array
    and sets options such as builder.clanlist_seen_threshold
     or subclasses and makes major changes to setup_builders() method
    """


    def __init__(self, workingdbfile, periods, max_session_date, select_hyenas_sql = None, clanlist_seen_threshold = 10, verbose=True):

        self.workingdbfile = workingdbfile
        self.db = sqlitedb.SqliteDB(workingdbfile)

        # options
        self.verbose = verbose
        self.periods = periods

        # not init params yet; set this after creating the object
        self.clan = 'talek'
        
        # these two have Python "property code," just because it seemed useful for validation
        self.clanlist_seen_threshold = clanlist_seen_threshold        
        self.max_session_date =  max_session_date   
        
        # this doesn't have a Python "propery" and can be set directly
        if (select_hyenas_sql):
            self.select_hyenas_sql = select_hyenas_sql
        else:
            self.select_hyenas_sql =  self.default_hyena_select()
        
        self.builders = self.default_builders()

        # self.builders[0].select_hyenas_sql = self.select_hyenas_sql


    def default_hyena_select(self):
        """all sna builders need way to select the animals in the study.
        This is a standard way to do this using study parameters 'clan' and 'max session date'"""
        # This is also set as a default in the Egos table builder, which is redundant
        select_hyenas_sql = """SELECT hyenas.hyena as hyena
            FROM hyenas
            WHERE   hyenas.birthdate is not null
                and hyenas.mom       is not null
                and hyenas.firstseen is not null
                and hyenas.dengrad   is not null
                and julianday(hyenas.firstseen) < julianday(date(hyenas.birthdate, "+90 days"))
                and strftime('%Y', hyenas.firstseen) >= '1989' and hyenas.clan='{0}'
                and julianday(hyenas.firstseen) < julianday('{1}')
            order by  hyenas.birthdate;
            """.format(self.clan, self.max_session_date)

        return(select_hyenas_sql)

    @property
    def builders(self):
        """builders is an array of table makers to be run (and tables built) in order"""
        return(self._builders)

    @builders.setter
    def builders(self, builder_array):
        """create an array of objects that make database tables and
        save as builders property"""
        self.builders = builder_array

    def append_builder(self, tablemaker_object):
        self._builders = self._builders.append(tablemaker_object)


    @property
    def max_session_date(self):
        """ max session date is used by many of the builders to limit sessions used"""
        return self._max_session_date
        
    @max_session_date.setter
    def max_session_date(self, value = None):
        """ max_session_date is the latest possible date to use for all tables,
        determined by the table with the least amount of data.  You are required
        to know this  date based on the database you are using, and set it here.
        Of the tables you require (aggressions, sessions, etc), use the """

        # future: use min session date in any table
        # examine all tables in db, check for "session" column,
        # lookup date of session,find min
        # challenge is don't know which tables are to be used
        self._max_session_date = value if value else "2013-03-30"
        
        
    def default_builders(self):
        """setup the builder class (that contain sql) we'll be using here.
        SNA database needs common set of tables to work with R code
        This default method is here mostly to provide an example of how to add
        an array of builders mydbbuilder.builders = [ ] """

        # these are in dependency order
        # HOW can we make this more flexible so that we can swap out one or two builders without
        # having to specify all of them

        # these are subclasses of TableMaker class that all have a .make_table() method.
        # this is an array of objects.
        # many of these need common parameters to run
        # db, periods,

        return  [SessionsPlusMaker(self.db, self.max_session_date),
                 EgonamesMaker(self.db, self.max_session_date, self.select_hyenas_sql, self.clan),
                 EgosTableMaker(self.db, self.max_session_date),
                 PeriodsTableMaker(self.db, self.periods),
                 LifePeriodsTableMaker(self.db, self.periods, self.max_session_date),
                 ClanListsMaker(self.db, self.clanlist_seen_threshold),
                 AIPairsMaker(self.db),
                 AItogethersessionsMaker(self.db),
                 AisElMaker(self.db),
                 GreetingsElMaker(self.db),
                 LiftlegbothElMaker(self.db),
                 LiftlegElMaker(self.db),
                 AggressionsElMaker(self.db),
                 DyadicAggressionsElMaker(self.db)
                ]


    def build_db(self):
        """invoke table making for each builder object in the builders array.
        the array must be in dependency order and  include all parameters
        needed by the builders"""

        result = True

        for builder in self.builders:
            # if self.verbose:
            #  print(builder.make_table_sql())

            # run tablemaker code... and test that it was made
            if(self.verbose):
                 print("/n********** building {0}".format(builder.tablename))
            result = builder.make_table()

            if not(result):
                raise RuntimeError("problem making table")

            if self.verbose:
                print( 'table{0}, {1} rows'.format( builder.tablename, self.db.table_rowcount(builder.tablename) ) )


        # clean up ; put this in SNABuilder class?
        self.db.close()

        return(result)

    def close_db(self):
        """closes the db connection for this class"""
        self.db.close()


    def print_result(self):
        """debugging"""
        print('---- resulting tables in {0} ----'.format(self.db.dbfile))
        print("\n".join(self.db.tables()))
