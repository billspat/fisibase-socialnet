#!/usr/bin/env python
"""example method to build and run sna db builder with params specific to this study"""

import argparse
from snabuilder import SNABuilder
import builder_utils

DBFILE = 'db/test.sqlite'
VERBOSE = True  # print lots of stuff

# -- make duration of period 2 & 3 the same num days as period 1
# -- previous this period was from dengrad to weaned,
# -- date(hyenas.weaned, '-1 days') as period_end
periods = [{'seq': "1",
            'name': 'cd',
            'desc': 'dfs to dengrad',
            'start_expr': "hyenas.firstseen",
            'end_expr': "date(hyenas.dengrad, '-1 day')",
            'where': None},
           {'seq': "2",
            'name': "postgrad",
            'desc': 'time after den graduation; duration equal time as period 1',
            'start_expr': "hyenas.dengrad",
            'end_expr': "date(hyenas.dengrad, '+' ||(julianday(hyenas.dengrad)-JulianDay(hyenas.firstseen)-1)|| ' days')",
            'where': None},
           {'seq': "3",
            'name': "adult",
            'desc': 'after 2 cal yrs of age; duration equal time as period 1',
            'start_expr': "date(hyenas.birthdate, '+2 years')",
            'end_expr': "date(hyenas.birthdate, '+' ||(365*2+(julianday(hyenas.dengrad)- JulianDay(hyenas.firstseen)-1))  ||' days')",
            'where': None}
          ]


if VERBOSE:
    print 'building sna tables in {0}'.format(DBFILE)

# create a builder object with required parameters
snatestbuilder = SNABuilder(DBFILE, periods)

# can set more options
snatestbuilder.clan = 'talek'
snatestbuilder.max_session_date = "2000-12-31"
snatestbuilder.clanlist_seen_threshold = 10
snatestbuilder.verbose = VERBOSE  # tells builder to print all SQL commands

# provide sql for how hyenas will be seleted for this study
# this is a very short time of project for testing purposes
snatestbuilder.select_hyenas_sql = """SELECT hyenas.hyena as hyena
        FROM hyenas
        WHERE   hyenas.birthdate is not null
            and hyenas.mom       is not null
            and hyenas.firstseen is not null
            and hyenas.dengrad   is not null
            and julianday(hyenas.firstseen) < julianday(date(hyenas.birthdate, "+90 days"))
            and strftime('%Y', hyenas.firstseen) >= '1995' and hyenas.clan='{0}'
            and julianday(hyenas.firstseen) < julianday('{1}')
        order by  hyenas.birthdate;
        """.format(snatestbuilder.clan, snatestbuilder.max_session_date)

# now that options are set, run the builder code
# see
result = snatestbuilder.build_db()
if VERBOSE:
    snatestbuilder.print_result()


# optional method for setting parameters: use command line interface to be run
# from the shell (or script to the HPC)
# python test_snabuilder.py -v mydatabasefile.sqlite
# send a valid file name for a db, one that has the required tables and no
# other SNA tables"""

# command_line_arguments.dbfile
# command_line_arguments.verbose


def getArguments():
    """short quick function to read command line args"""
    parser = argparse.ArgumentParser()
    parser.add_argument('dbfile',
                        help='initial database with fisibase tables (required)',
                        type=builder_utils.valid_file)

    parser.add_argument('--verbose', '-v', required=False, help='print progress on command line',
                        action="store_true")

    command_line_arguments = parser.parse_args()
    return(command_line_arguments)
