#!/usr/bin/env python

import os
import argparse
from snabuilder import SNABuilder


def sixmonth_db_builder(dbfile):
    """factory method for snb_dbuilder with params specific to this study"""

    periods = [
        {'seq': 1, 'name': '6to12', 'desc': '6 months old to 1 year old',
         'start_expr': "date(hyenas.birthdate, '+6 months')",
         'end_expr': "date(hyenas.birthdate, '+1 year')",
         'where': None},
        {'seq': 2, 'name': '12to18', 'desc': '1 year old to 18 months',
         'start_expr': "date(hyenas.birthdate, '+1 year')",
         'end_expr': "date(hyenas.birthdate, '+18 months')",
         'where': None},
        {'seq': 3, 'name': '18to24', 'desc': '18 months old to 2 yrs',
         'start_expr': "date(hyenas.birthdate, '+18 months')",
         'end_expr': "date(hyenas.birthdate, '+24 months')",
         'where': None},
        {'seq': 4, 'name': '24to30', 'desc': '2 yrs old to 2.5 years old',
         'start_expr': "date(hyenas.birthdate, '+24 months')",
         'end_expr': "date(hyenas.birthdate, '+30 months')",
         'where': None}]

    
    # options
    clan = 'talek'
    max_session_date = "2013-09-30"  # current db as of
    clanlist_seen_threshold = 10
    # provide sql for how hyenas will be seleted for this study
    select_hyenas_sql = """SELECT hyenas.hyena as hyena
            FROM hyenas
            WHERE   hyenas.birthdate is not null
                and hyenas.mom       is not null
                and hyenas.firstseen is not null
                and hyenas.dengrad   is not null
                and julianday(hyenas.firstseen) < julianday(date(hyenas.birthdate, "+90 days"))
                and strftime('%Y', hyenas.firstseen) >= '1989' and hyenas.clan='{0}'
                and julianday(hyenas.firstseen) < julianday('{1}')
            order by  hyenas.birthdate;
            """.format(clan, max_session_date)
            
    dbbuilder = SNABuilder(dbfile, periods, max_session_date, select_hyenas_sql, clanlist_seen_threshold)
            
    return(dbbuilder)


def valid_file(filepath):
    if not os.path.exists(filepath):
        msg = "can't find database file %s" % filepath
        raise argparse.ArgumentTypeError(msg)
    return filepath


if __name__ == "__main__":
    """ send a valid file name for a db, one that has the required tables and no other SNA tables"""
    parser = argparse.ArgumentParser()

    parser.add_argument('dbfile',
                        help='initial database with fisibase tables (required)',
                        type=valid_file ) 

    parser.add_argument('--verbose', '-v', required=False, help='print progress on command line',
                        action="store_true")
    myargs = parser.parse_args()

    if myargs.verbose:
         print('building sna tables in {0}'.format(myargs.dbfile))
    
    builder = sixmonth_db_builder(myargs.dbfile)
    result = builder.build_db()
    if myargs.verbose:
        builder.print_result()


    