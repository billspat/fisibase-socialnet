#!/usr/bin/env python

# socialnet_build.py
# this is an obsolete program to build the SNA database
# superceded by subclassing snabuilder.py with parameters specific to a kind of study
# and that uses builders in tablebuilders.py
# this should be eventually deleted...

import os, shutil
import argparse
from sqlitedb import SqliteDB, TableMaker, sqlite_versions
import sqlitedb

CLANLIST_SEEN_THRESHOLD = 10
MAX_SESSION_DATE = "2013-03-30"
MAKE_AI = True  # this is an argument, but set as constant here too

def behavior_index_ddl(tablename):
    """SQL for standard indexing on edge list tables like this sql = behavior_index_ddl(self.tablename) """
    return("CREATE UNIQUE INDEX '{0}-edge' ON {0} (ego, period, H1, H2);".format(tablename))

def build_db(db, make_ai = MAKE_AI, verbose = True ):
    """call each table making class to make one table at time in the db, 
        optionally build the really big tables. """
       
    table_classes = [
        SessionsPlusMaker,
        EgosTableMaker,
        PeriodsTableMaker,
        LifePeriodsTableMaker,
        ClanListsMaker,
        GreetingsElMaker,
        LiftlegbothElMaker,
        LiftlegElMaker,
        AggressionsElMaker,
        DyadicAggressionsElMaker]

    # AIs take the longest right so do this only if this flag is set
    # by adding these AI tables to the list of tables to make    
    if (make_ai):
        table_classes.extend([AIPairsMaker,AItogethersessionsMaker,AisElMaker])

        
    for tm_class in table_classes:
        tm = tm_class(db)
        if not(tm.make_table()):
            raise RuntimeError("problem making table")
        if verbose:
            print( 'table{0}, {1} rows'.format( tm.tablename, db.table_rowcount(tm.tablename) ) ) 

    # normalize edge weights by AI-minutes 
    if (make_ai):
        normalize_edge_weights(db)
        print( ' edge weights normalized')        
    return(True)

def build_partial_egonames(db,start_alpha=None,end_alpha=None ):
    """this is used to build just a portion of all the egos needed to allow for multiples jobs to be run"""
    if start_alpha is None:
        start_alpha = 'a'
    if end_alpha  is None:
        end_alpha   = 'z'
    
    tm = EgonamesMaker(db,start_alpha,end_alpha)
    print(tm.tablename)
    print(tm.make_table())

#     """create  egonames table, and find individuals that meet criteria for inclusion,  but limit to specifi c
#    alphabet for chunking data workflow """

class EgonamesMaker(TableMaker):
    """ names of animals that meet inclusion criteria"""
    def __init__(self, db, start_alpha=None, end_alpha=None):
        TableMaker.__init__(self,db)
        self.tablename = 'egonames'
        self.table_sql = """CREATE TABLE egonames (hyena varchar, PRIMARY KEY(hyena));"""

        if start_alpha is None:
            self.start_alpha = 'a'
        else:
            self.start_alpha = start_alpha.lower()
            
        if end_alpha  is None:
            self.end_alpha   = 'z'
        else:
            self.end_alpha = end_alpha.lower()
        
        # to make this sort below, add z's to the single letter
        self.end_alpha = self.end_alpha + "zzzz"
            
    def make_table_sql(self):
        return(self.table_sql)
        
    def insert_records_sql(self):
        insert_sql="""INSERT INTO egonames (hyena) 
            SELECT hyenas.hyena as hyena
            FROM hyenas inner join hyenas as moms on hyenas.mom = moms.hyena
            where hyenas.birthdate   is not null 
                and hyenas.firstseen is not null 
                and hyenas.dengrad   is not null 
                and julianday(hyenas.firstseen) < julianday(date(hyenas.birthdate, "+90 days")) 
                and strftime('%Y', hyenas.firstseen) >= '1989' and hyenas.clan='talek' 
                and hyenas.hyena > '{0}' and hyenas.hyena <= '{1}'
            order by  hyenas.birthdate;""".format(self.start_alpha,self.end_alpha)
            
            #and weaned    is not null  # someday may need wean dates
        return(insert_sql)
       
       
#-----------------------------------
class SessionsPlusMaker(TableMaker):
    """create  SessionsPlus table, based on sessions but filtered"""
    
    def __init__(self, db):
        TableMaker.__init__(self,db)
        # super(SessionsPlusMaker,self).__init__(db)
        self.tablename = 'sessions_plus'

    def make_table_sql(self):
        sql="""CREATE TABLE sessions_plus AS
        SELECT sessions.session as session,
                location, sessiondate, start, stop,
                unidhyenas, other,
                sessions.tracked as tracked,
                seen, pickup,
                ("," || group_concat(hyenaspersession.hyena) || ",")
                    AS hyenas,
                count(hyenaspersession.hyena)
                      as identified_hyena_count,
                (strftime('%s',sessions.stop ) - strftime('%s',sessions.start) ) /60
                    as sessionminutes,  -- eg. DURATION
                max(hyenaspersession.fas)
                    as fas,
                "talek"
                    as clan
        FROM
            sessions INNER JOIN hyenaspersession ON sessions.SESSION = hyenaspersession.session
        -- exclude sessions that cross midnight boundary and give negative durations
        WHERE (strftime('%s',sessions.stop ) - strftime('%%s',sessions.start) ) > 0
           AND (julianday(sessions.sessiondate) <= julianday('{0}') )
        GROUP BY sessions.session
        ORDER BY sessions.sessiondate, sessions.START;""".format(MAX_SESSION_DATE)

        return(sql)

    def index_sql_array(self):
        return(
            ["CREATE UNIQUE INDEX pk_sessions_plus         on sessions_plus (session);",
             "CREATE        INDEX idx_sessions_plus_date   on sessions_plus (sessiondate);",
             "CREATE        INDEX idx_sessions_plus_hyenas on sessions_plus (hyenas);"]     )


class EgosTableMaker(TableMaker):
    """create  egos table"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        self.tablename = 'egos'
        age_last_seen_table_sql = """CREATE temp table age_last_seen as 
             SELECT  hyenas.hyena as hyena, round((julianday(max(sessions_plus.sessiondate)) - julianday(hyenas.birthdate))/365.0,2) as age_at_last_seen,
             max(sessions_plus.sessiondate) as lastsession
             FROM ((sessions_plus inner join hyenaspersession on sessions_plus.session = hyenaspersession.session)
                inner join hyenas    on hyenas.hyena = hyenaspersession.hyena ) 
                inner join egonames on hyenas.hyena = egonames.hyena
        group by hyenas.hyena;"""
        
        # need a temp table with ego and data in it to do the update becuase using agregate function...
        db.execddl("drop table if exists age_last_seen;")
        db.execddl(age_last_seen_table_sql)
        

            
        
    def make_table_sql(self):        
        table_sql="""CREATE TABLE egos (
               ego               varchar PRIMARY KEY, 
               sex               varchar,  -- from hyenas table
               mom               varchar,  -- from hyenas table
               birthdate         date,
               firstseen         date,
               disappeared       date,     
               lastsession       date,
               dengrad           date,
               fate              varchar,
               clan              varchar,
               longevity_days    integer,
               age_at_last_seen  real, 
               annual_rs         real, 
               age_at_dispersal  real 
            ); """
        
        return(table_sql)

    def insert_records_sql(self):
        """ build full egos table from hyenas and AgeLastSeen table temp """
        sql="""INSERT INTO egos (
                ego,
                sex,
                mom,
                birthdate,
                firstseen,
                disappeared,
                lastsession,
                dengrad  ,
                fate,
                clan     ,
                longevity_days,
                age_at_last_seen,
                age_at_dispersal )
            SELECT 
                hyenas.hyena as ego, 
                hyenas.sex      , 
                hyenas.mom      , 
                hyenas.birthdate,
                hyenas.firstseen,
                hyenas.disappeared,
                age_last_seen.lastsession,
                hyenas.dengrad  ,
                hyenas.fate , 
                hyenas.clan     ,
                ( julianday(hyenas.disappeared) - julianday(hyenas.birthdate) ) as longevity_days,
                age_last_seen.age_at_last_seen as age_at_last_seen,
                CASE 
                    WHEN hyenas.sex like 'm' and 
                         hyenas.disappeared is not null and 
                         hyenas.fate like 'd' 
                    THEN  age_last_seen.age_at_last_seen ELSE NULL END 
                    as age_at_dispersal 
            FROM age_last_seen inner join hyenas 
                 on age_last_seen.hyena = hyenas.hyena;
            """
        return(sql)

    def bonus_sql(self):
        """calculate a reproductive success values for females, and update rs_days column. 
            Requires temporary table to build these values up, and then copied into the """
                
        repro_success_sql = """CREATE TEMP TABLE repro_success AS SELECT moms.id as hyena, 
            count(cubs.id) as cubcount, 
            moms.birthdate, min(cubs.birthdate) as firstlitter, 
            max(cubs.birthdate) as lastlitter, moms.disappeared, 
            round ( CASE
       WHEN moms.disappeared is null THEN 
           -- alive, use recent date
           count(cubs.id)/((julianday("2015-04-01") - julianday( min(cubs.birthdate)) )/365)
       WHEN julianday(max(cubs.birthdate)) - julianday(moms.disappeared) < 365 THEN 
           -- died < 1 year soon after last litter, normalize to litter + 1 year)
           count(cubs.id)/((julianday(max(cubs.birthdate))+365 - julianday( min(cubs.birthdate)) )/365)
       ELSE 
           -- lived past litter + 10 months, use actual disappear date  
           count(cubs.id)/((julianday(moms.disappeared) - julianday( min(cubs.birthdate)) )/365)
       END,3) AS annual_repro_sucess
       from hyenas  as cubs inner join hyenas as moms on cubs.mom = moms.id
       where moms.birthdate is not null and moms.clan = 'talek' and moms.id in (select ego from egos) 
            and moms.id != 'hk' and moms.id != 'cen'
       group by moms.id
"""
# end of repro_success_sql


        db.execddl("drop table if exists repro_success;")
        db.execddl(repro_success_sql)

        update_repro_success_sql = """UPDATE egos SET 
         annual_rs = ( select repro_success.annual_repro_sucess
            FROM repro_success
                     WHERE repro_success.hyena = egos.ego);"""
                
        return(update_repro_success_sql)        
    
    
    
class PeriodsTableMaker(TableMaker):
    """create  Periods table"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        self.tablename = 'periods'
    def make_table_sql(self):
        sql="""CREATE TABLE periods (
             seq integer,
             period varchar,
             period_desc varchar,
             PRIMARY KEY(period) );"""
        return(sql)
    def insert_records_sql(self):
        insert_sql="""INSERT INTO periods (seq, period,period_desc)
            VALUES 
            (1, 'cd', 'dfs to dengrad'),
            (2, 'postgrad', 'time after den graduation; duration equal time as period 1'),
            (3, 'adult', 'after 2 cal yrs of age; duration equal time as period 1' ),
            (4, '6to12','6 months old to 1 year old'),
            (5, '12to18',' 1 year old to 18 months'),
            (6, '18to24','18 months old to 2 yrs'),
            (7, '24to30','2 yrs old to 2.5 years old'),
            (8, '12mBD','12th month before dispersal'),
            (9, '11mBD','11th month before dispersal'),
            (10,'10mBD','10th month before dispersal'),
            (11,'9mBD', '9th month before dispersal'),
            (12,'8mBD', '8th month before dispersal'),
            (13,'7mBD', '7th month before dispersal'),
            (14,'6mBD', '6th month before dispersal'),
            (15,'5mBD', '5th month before dispersal'),
            (16,'4mBD', '4th month before dispersal'),
            (17,'3mBD', '3th month before dispersal'),
            (18,'2mBD', '2th month before dispersal'),
            (19,'1mBD', '1th month before dispersal')
            ; """
        return(insert_sql)
 

class LifePeriodsTableMaker(TableMaker):
    """create  LifePeriods table.  Note this table has multiple UPDATE sql at the end"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        self.tablename = 'lifeperiods'
        # sql fragements used in union - no semicolons here
        
        self.table_sql ="""CREATE TABLE lifeperiods (
             ego              varchar,
             seq              integer,
             period           varchar,
             period_start     date,
             period_end       date,
             ego_period_rank  real,
             mom_period_rank  real,
             clan_size        integer,
             sessions_count   integer,
             sessions_alone   integer,
             mom_alive        integer,
             prey_level       varchar,
             PRIMARY KEY(ego,period)
         );
        """
    
    def make_insert_sql(self):
            template = """SELECT
               egos.ego, 
               {seq}         as seq, 
              '{periodname}' as period,
               {start_expr}  as period_start,
               {end_expr}    as period_end,
               0 as sessions_count, 0 as sessions_alone, NULL as mom_alive, NULL as prey_level
            FROM hyenas inner join egos on hyenas.hyena = egos.ego
            WHERE {where}"""
            
            periods = {}
            periods[1] = {'seq'      : "1", 
                    'periodname': "cd", 
                    'start_expr': "hyenas.firstseen", 
                    'end_expr'  : "date(hyenas.dengrad, '-1 day')" ,
                    'where' : "1=1"}
   
            # -- make duration of period 2 & 3 the same num days as period 1
            # -- previous this period was from dengrad to weaned,
            # -- date(hyenas.weaned, '-1 days') as period_end        
            periods[2] = {'seq'      : "2", 
                    'periodname': "postgrad", 
                    'start_expr': "hyenas.dengrad", 
                    'end_expr'  : "date(hyenas.dengrad, '+' ||(julianday(hyenas.dengrad)-JulianDay(hyenas.firstseen)-1)|| ' days')" ,
                    'where' : "1=1"}
   

            periods[3] = {'seq'      : "3", 
                    'periodname': "adult", 
                    'start_expr': "date(hyenas.birthdate, '+2 years')", 
                    'end_expr'  : "date(hyenas.birthdate, '+' ||(365*2+(julianday(hyenas.dengrad)- JulianDay(hyenas.firstseen)-1))  ||' days')" ,
                    'where' : "1=1"}
   

    # new periods 'arbitrary' 6 moth periods

            periods[4] = {'seq'      : "4", 
                    'periodname': "6to12", 
                    'start_expr': "date(hyenas.birthdate, '+6 months')", 
                    'end_expr'  : "date(hyenas.birthdate, '+1 year')" ,
                    'where' : "1=1"}
   
            periods[5] = {'seq'      : "5", 
                    'periodname': "12to18", 
                    'start_expr': "date(hyenas.birthdate, '+1 year')", 
                    'end_expr'  : "date(hyenas.birthdate, '+18 months')" ,
                    'where' : "1=1"}
   
            periods[6] = {'seq'      : "6", 
                    'periodname': "18to24", 
                    'start_expr': "date(hyenas.birthdate, '+18 months')", 
                    'end_expr'  : "date(hyenas.birthdate, '+24 months')" ,
                    'where' : "1=1"}
    
            periods[7] = {'seq'      : "7", 
                    'periodname': "24to30", 
                    'start_expr': "date(hyenas.birthdate, '+24 months')", 
                    'end_expr'  : "date(hyenas.birthdate, '+30 months')" ,
                    'where' : "1=1"}
    
    
    # male dispersal, 12 30 day periods backward from dispersal 
            periods[8] = {'seq' : "8",
                'periodname': "12mBD",
                'start_expr':"date(hyenas.disappeared, '-360 days')",
                'end_expr'  :"date(hyenas.disappeared, '-331 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}

            periods[9] = {'seq' : '9',
                'periodname': '11mBD',
                'start_expr':"date(hyenas.disappeared, '-330 days')",
                'end_expr'  :"date(hyenas.disappeared, '-301 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}

            periods[10] = {'seq' : '10',
                'periodname': '10mBD',
                'start_expr':"date(hyenas.disappeared, '-300 days')",
                'end_expr'  :"date(hyenas.disappeared, '-271 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}

            periods[11] = {'seq' : '11',
                'periodname': '9mBD',
                'start_expr':"date(hyenas.disappeared, '-270 days')",
                'end_expr'  :"date(hyenas.disappeared, '-241 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}

            periods[12] = {'seq' : '12',
                'periodname': '8mBD',
                'start_expr':"date(hyenas.disappeared, '-240 days')",
                'end_expr'  :"date(hyenas.disappeared, '-211 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}

            periods[13] = {'seq' : '13',
                'periodname': '7mBD',
                'start_expr':"date(hyenas.disappeared, '-210 days')",
                'end_expr'  :"date(hyenas.disappeared, '-181 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}

            periods[14] = {'seq' : '14',
                'periodname': '6mBD',
                'start_expr':"date(hyenas.disappeared, '-180 days')",
                'end_expr'  :"date(hyenas.disappeared, '-151 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}

            periods[15] = {'seq' : '15',
                'periodname': '5mBD',
                'start_expr':"date(hyenas.disappeared, '-150 days')",
                'end_expr'  :"date(hyenas.disappeared, '-121 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}

            periods[16] = {'seq' : '16',
                'periodname': '4mBD',
                'start_expr':"date(hyenas.disappeared, '-120 days')",
                'end_expr'  :"date(hyenas.disappeared, '-91 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}

            periods[17] = {'seq' : '17',
                'periodname': '3mBD',
                'start_expr':"date(hyenas.disappeared, '-90 days')",
                'end_expr'  :"date(hyenas.disappeared, '-61 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}

            periods[18] = {'seq' : '18',
                'periodname': '2mBD',
                'start_expr':"date(hyenas.disappeared, '-60 days')",
                'end_expr'  :"date(hyenas.disappeared, '-31 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}

            periods[19] = {'seq' : '19',
                'periodname': '1mBD',
                'start_expr':"date(hyenas.disappeared, '-30 days')",
                'end_expr'  :"date(hyenas.disappeared, '-1 days')",
                    'where' : "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}
                
            insert_sql ="""insert into lifeperiods 
                (ego,seq, period,period_start,period_end, 
                    sessions_count, sessions_alone, mom_alive,prey_level) """
            insert_sql = insert_sql +  "\nUNION \n".join([ template.format(**periods[i]) for i in range(1,len(periods)+1)]) 
            insert_sql = insert_sql + ";"        
            return(insert_sql)
            
    def make_table_sql(self):
        return(self.table_sql)

    def insert_records_sql(self):
        """ this unions the select statements defined in init above """
        return(self.make_insert_sql())
        
    def index_sql_array(self):
        return(
            ["CREATE INDEX idx_period_start on lifeperiods  (period_start);"]
         )
    
    def bonus_sql(self):
        """ this was to be simply added sql, but here calling the db.exec command
        as a side effect because there are three SQL DDL to call for this table"""
        
        # -- mom's rank during the period added to this table for reference
        sql = """UPDATE lifeperiods 
        SET mom_period_rank = 
            (SELECT  ranks.stdrelativerank as momrank 
            FROM (ranks inner join egos on ranks.hyena = egos.mom ) 
            WHERE lifeperiods.ego = egos.ego
              and ranks.year <= strftime("%Y", period_end  ) 
              and ranks.year >= strftime("%Y", period_start)
            GROUP BY ranks.hyena
        );
        """ 
        # hack: exec sql here 
        db.execddl(sql)
    
        # -- when animal is young does not have a rank, so insert the mom's rank
        sql = """UPDATE lifeperiods
            SET ego_period_rank = mom_period_rank 
            WHERE ego_period_rank is null;"""     
        # hack: exec sql here rather than pass it?          
        db.execddl(sql)
        
        ### remove those egos that have one or more null ranks (e.g mom rank is null)
        ## no rank means not part of the clan, so don't include them

        delete_sql = """DELETE FROM lifeperiods 
            WHERE ego IN 
                (SELECT DISTINCT ego 
                    from lifeperiods 
                    where lifeperiods.mom_period_rank is null
                );"""
        db.execddl(delete_sql)    
        
         
        mom_alive_sql = """UPDATE lifeperiods SET mom_alive = 
            (SELECT CASE 
                    when moms.disappeared is null then 1 
                    when julianday(moms.disappeared) > 
                         julianday(lifeperiods.period_start) + 
                         ( julianday(lifeperiods.period_end) -  julianday(lifeperiods.period_start)) / 2
                         then 1
                    else 0 
                    END as mom_alive
            FROM hyenas inner join hyenas as moms on hyenas.mom = moms.hyena
            WHERE lifeperiods.ego = hyenas.hyena);        
        """ 
        db.execddl(mom_alive_sql)    
        
        # ADD COUNTING of hyenas in sessions here
        # count all sessions 
        # session_alone_count = """update SELECT count(sessions_plus.session) as sessions_not_alone FROM lifeperiods, sessions where ego in session (?), unids is null, session_hyenas count = 1        
        # """"
        
        # create temporary in-memory tables to hold calculations
        
        temp_session_count_sql = """CREATE temp TABLE session_counts as 
select lifeperiods.ego as ego, lifeperiods.period as period, count(sessions_plus.session) as session_count from (sessions_plus inner join hyenaspersession on sessions_plus.session = hyenaspersession.session ) inner join lifeperiods on lifeperiods.ego = hyenaspersession.hyena   where sessions_plus.sessiondate >= lifeperiods.period_start and sessions_plus.sessiondate <= lifeperiods.period_end
group by lifeperiods.ego, lifeperiods.period;"""
        
        temp_alone_session_count_sql = """CREATE temp TABLE alone_session_counts as 
        select lifeperiods.ego as ego, lifeperiods.period as period, count(sessions_plus.session) as alone_session_count from lifeperiods, sessions_plus
         where sessions_plus.sessiondate >= lifeperiods.period_start 
               and sessions_plus.sessiondate <= lifeperiods.period_end 
               and sessions_plus.hyenas= ','||lifeperiods.ego||','
        group by lifeperiods.ego, lifeperiods.period;"""

        
        update_session_count_sql = """UPDATE lifeperiods set sessions_count = 
         (select session_counts.session_count from session_counts 
         where   lifeperiods.ego    = session_counts.ego
             and lifeperiods.period = session_counts.period);"""
        update_alone_session_count_sql = """UPDATE lifeperiods set sessions_alone = 
          (select alone_session_counts.alone_session_count from alone_session_counts 
          where   lifeperiods.ego    = alone_session_counts.ego
              and lifeperiods.period = alone_session_counts.period);"""

        db.execddl("""drop table if exists session_counts;""")
        db.execddl(temp_session_count_sql)

        db.execddl("""drop table if exists alone_session_counts;""")
        db.execddl(update_session_count_sql)
         
        db.execddl(temp_alone_session_count_sql)
        db.execddl(update_alone_session_count_sql)
         
        ### need to add  clan_size
        
        # fill in preylevels column nominal of prey avail
        # compare average total prey for all overlapping census periods
        # with average prey and 
         
        temp_preylevels_sql = """CREATE TEMP TABLE preylevels  
            as SELECT 
               lifeperiods.period as period,
               lifeperiods.ego as ego,
               avg(preytransects.totalprey) as periodtotalprey,
               avgprey.avgprey,
               case WHEN avg(preytransects.totalprey) > avgprey 
                   THEN 'high'
                   ELSE 'low'
               END AS prey_level,
               lifeperiods.period_start,
               lifeperiods.period_end
         FROM preytransects inner join avgprey on preytransects.park = avgprey.park,
               lifeperiods
         WHERE (preytransects.stopdate >= lifeperiods.period_start AND 
                preytransects.startdate < lifeperiods.period_end) AND 
               preytransects.park = 'N'
         GROUP BY lifeperiods.period,
                  lifeperiods.ego;"""
           
        update_preylevels_sql = """UPDATE lifeperiods set prey_level = 
         (select prey_level from preylevels 
           where lifeperiods.ego    = preylevels.ego
             and lifeperiods.period = preylevels.period);"""
             
        db.execddl("""drop table if exists preylevels;""")
        db.execddl(temp_preylevels_sql)
        db.execddl(update_preylevels_sql)
        
        dummysql = """select 1"""
            
        # pass dummysql to table builder worker; we've already done all the work here...
        return(dummysql)


class ClanListsMaker(TableMaker):
    """create  lifeperiods_clanlists table"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        self.tablename = 'lifeperiods_clanlists'
        self.table_sql = """CREATE TABLE lifeperiods_clanlists AS 
            SELECT distinct
                  lifeperiods.ego          as ego,
                  lifeperiods.period       as period,
                  lifeperiods.period_start as period_start,
                  lifeperiods.period_end   as period_end,
                  hyenaspersession.hyena   as hyena,
                  0                        as hyenarank,
                  hyenas.sex               as sex,
                  (julianday(lifeperiods.period_start)-julianday(hyenas.birthdate)) 
                                           as hyenaAgeAtStart,
                  count(sessions_plus.session)
                                           as sessions_seen,
                  sum((strftime('%s',sessions_plus.stop)-strftime('%s',sessions_plus.start) ) /60.0) 
                                           as minutes_seen
            FROM (hyenaspersession 
                  inner join sessions_plus on hyenaspersession.session = sessions_plus.session)
                  inner join hyenas on hyenaspersession.hyena = hyenas.hyena,
                  lifeperiods
            WHERE 
                  SESSIONDATE between lifeperiods.period_start and lifeperiods.period_end
                  and hyenas.clan like 'talek%'
            GROUP BY 
                lifeperiods.ego, lifeperiods.period, hyenaspersession.hyena
            HAVING 
                sessions_seen >= {0};""".format(CLANLIST_SEEN_THRESHOLD)

            # CONSTANT  CLANLIST_SEEN_THRESHOLD SET AT TOP OF THIS FILE
        
    def make_table_sql(self):
        return(self.table_sql)

    def index_sql_array(self):
        return( ["""CREATE UNIQUE INDEX idx_lifeperiods_clanlists 
                    ON lifeperiods_clanlists (ego,period,hyena);""", 
                """CREATE INDEX idx_lifeperiods_clanlists_dates 
                    ON lifeperiods_clanlists (period_start,period_end,hyena);""", 
                """CREATE INDEX idx_lifeperiods_clanlists_period 
                    ON lifeperiods_clanlists (period);""",
                """CREATE INDEX idx_lifeperiods_clanlists_ego 
                    ON lifeperiods_clanlists (ego);"""
                ])

    def bonus_sql(self):
        # now that clanlists are done, can count them and update clansize...
        calc_clansize_sql = """UPDATE lifeperiods set clan_size = 
         (select count(lifeperiods_clanlists.hyena) as clan_size from lifeperiods_clanlists 
         where lifeperiods.ego = lifeperiods_clanlists.ego and  lifeperiods.period = lifeperiods_clanlists.period
         group by lifeperiods_clanlists.ego, lifeperiods_clanlists.period); """
        return(calc_clansize_sql)
         
        

############## EDGE LISTS #############


class GreetingsElMaker(TableMaker):
    """create  greetings_el table"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        self.tablename = 'lifeperiods_greetings_el'
        self.alt_table_sql = """CREATE TABLE lifeperiods_greetings_el (ego TEXT, period TEXT, period_start NUM, period_end NUM, H1, H2, weight, greet_role VARCHAR);"""
        
        self.table_sql = """CREATE TABLE  'lifeperiods_greetings_el'  AS
            SELECT 
                lifeperiods.ego as ego,
                lifeperiods.period as period,
                lifeperiods.period_start as period_start,
                lifeperiods.period_end as period_end,
                CASE WHEN greetings.ID1 < greetings.ID2 THEN greetings.ID1 ELSE greetings.ID2 END AS H1,
                CASE WHEN greetings.ID1 < greetings.ID2 THEN greetings.ID2 ELSE ID1 END AS H2, 
                count(*) AS weight,
                '' as greet_role
            FROM greetings,lifeperiods
            WHERE 
                 greetings.sessiondate BETWEEN lifeperiods.period_start AND lifeperiods.period_end
                 and greetings.ID1 is not null and greetings.ID2 is not null
                 and GRTOccured = "y"
                 and greetings.ID1 in (select lifeperiods_clanlists.hyena from
                      lifeperiods_clanlists 
                      where lifeperiods.period =  lifeperiods_clanlists.period
                      and lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
                 and greetings.ID2 in (select lifeperiods_clanlists.hyena from
                      lifeperiods_clanlists 
                      where lifeperiods.period =  lifeperiods_clanlists.period
                      and lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
            group by  lifeperiods.ego,lifeperiods.period, lifeperiods.period_start,H1,H2;
            """
            
    def make_table_sql(self):
        return(self.table_sql)
    
        
    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        index_sql = behavior_index_ddl(self.tablename)
        return([index_sql])
        
    
class LiftlegElMaker(TableMaker):
    """create  ll_el table - seperate create table and inserts"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        self.tablename = 'lifeperiods_ll_el'

    def make_table_sql(self):
        sql = """CREATE TABLE lifeperiods_ll_el (ego VARCHAR, period varchar, period_start DATE, 
                period_end DATE, H1 varchar, H2 varchar, weight INT, greet_role VARCHAR);"""
        return(sql)
            
    def insert_records_sql(self):
        sql="""INSERT INTO 'lifeperiods_ll_el' 
            (ego, period, period_start, period_end, H1, H2, weight, greet_role)
        SELECT 
             lifeperiods.ego as ego,
             lifeperiods.period as period,
             lifeperiods.period_start as period_start,
             lifeperiods.period_end as period_end,
             greetings.ll_solicitor as H1, 
             greetings.ll_reciever as H2, 
             count(*) as weight,
             ll_symmetry
        FROM greetings, lifeperiods 
        WHERE 
             greetings.sessiondate BETWEEN lifeperiods.period_start AND lifeperiods.period_end
             and H1 is not null and H2 is not null 
             and greetings.ll_solicitor in (select lifeperiods_clanlists.hyena from
                      lifeperiods_clanlists 
                      where lifeperiods.period =  lifeperiods_clanlists.period
                      and lifeperiods.ego =  lifeperiods_clanlists.ego
                 )  
             and greetings.ll_reciever  in (select lifeperiods_clanlists.hyena from
                      lifeperiods_clanlists 
                      where lifeperiods.period =  lifeperiods_clanlists.period
                      and lifeperiods.ego =  lifeperiods_clanlists.ego
                 )  
        group by  lifeperiods.ego, lifeperiods.period, lifeperiods.period_start,H1,H2;"""
        return(sql)

    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        index_sql = behavior_index_ddl(self.tablename)
        return([index_sql])
        
    ### FILTER BY ONLY ANIMALS IN THE CLAN LIST FOR THAT PERIOD

    ## add column for 'edge type'  = igraph edge attribute = solictor when H1 is solictor 
    ### second pass where only greetings.ll_symmetry = 'both' and
    ##          greetings.ll_reciever as H1,  
    ###         greetings.ll_solicitor as H2, 
    ##          "reciprocator" as greet_role

 


class LiftlegbothElMaker(TableMaker):
    """create lifeperiods_liftlegboth_el table"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        self.tablename = 'lifeperiods_liftlegboth_el'
                    
    def make_table_sql(self):
        sql = """CREATE TABLE lifeperiods_liftlegboth_el (ego VARCHAR, period varchar, period_start DATE, 
                period_end DATE, H1 varchar, H2 varchar, weight INT, greet_role VARCHAR);"""
        return(sql)
        
    def insert_records_sql(self):
        sql="""INSERT INTO 'lifeperiods_liftlegboth_el' 
            (ego, period, period_start, period_end, H1, H2, weight, greet_role)
        SELECT 
             lifeperiods.ego as ego,
             lifeperiods.period as period,
             lifeperiods.period_start as period_start,
             lifeperiods.period_end as period_end,
             greetings.ll_solicitor as H1,  
             greetings.ll_reciever as H2, 
             count(*) as weight,
             ' ' as greet_role
        FROM greetings, lifeperiods 
        WHERE 
             greetings.sessiondate BETWEEN lifeperiods.period_start AND lifeperiods.period_end
             and H1 is not null and H2 is not null and greetings.ll_symmetry = 'both'
             and greetings.ll_solicitor in (select lifeperiods_clanlists.hyena from
                      lifeperiods_clanlists 
                      where lifeperiods.period =  lifeperiods_clanlists.period
                      and lifeperiods.ego =  lifeperiods_clanlists.ego
                 )  
             and greetings.ll_reciever  in (select lifeperiods_clanlists.hyena from
                      lifeperiods_clanlists 
                      where lifeperiods.period =  lifeperiods_clanlists.period
                      and lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
        group by  lifeperiods.ego, lifeperiods.period, lifeperiods.period_start,H1,H2;"""
        return(sql)

    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        index_sql = behavior_index_ddl(self.tablename)
        return([index_sql])

    
class AggressionsElMaker(TableMaker):
    """create aggressions_el table"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        self.tablename = 'lifeperiods_aggressions_el'
                    
    def make_table_sql(self):
        sql = """CREATE TABLE lifeperiods_aggressions_el (
            ego VARCHAR, 
            period varchar, 
            period_start DATE, 
            period_end DATE, 
            H1 varchar, 
            H2 varchar, 
            weight FLOAT, 
            in_group varchar,
            context varchar);"""
        return(sql)
        
    def insert_records_sql(self):
        sql="""INSERT INTO 'lifeperiods_aggressions_el' 
                (ego, period, period_start, period_end, H1, H2, weight,in_group,context)
                select 
                 lifeperiods.ego as ego,
                 lifeperiods.period as period,
                 lifeperiods.period_start as period_start,
                 lifeperiods.period_end as period_end,
                 aggressions.aggressor as H1, 
                 aggressions.recip as H2, 
                 avg(aggressions.behavior1) as weight,
                 CASE 
                     WHEN aggressions.isgroup = 'Y' 
                     THEN 'Y'ELSE 'N' END  
                         as in_group,
                 aggressions.context as context
            from aggressions, lifeperiods
            where 
                    aggressions.sessiondate is not null
                and aggressions.sessiondate BETWEEN lifeperiods.period_start AND lifeperiods.period_end
                and aggressions.aggressor in (select lifeperiods_clanlists.hyena from
                      lifeperiods_clanlists 
                      where lifeperiods.period =  lifeperiods_clanlists.period
                      and lifeperiods.ego =  lifeperiods_clanlists.ego
                 ) 
                and aggressions.recip     in (select lifeperiods_clanlists.hyena from
                      lifeperiods_clanlists 
                      where lifeperiods.period =  lifeperiods_clanlists.period
                      and lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
            group by 
                lifeperiods.ego,lifeperiods.period, lifeperiods.period_start,
                H1,H2;"""
        return(sql)

    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        index_sql = behavior_index_ddl(self.tablename)
        return([index_sql])

class DyadicAggressionsElMaker(TableMaker):
    """create dyadic aggressions_el table"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        self.tablename = 'lifeperiods_dyadicaggressions_el'
                    
    def make_table_sql(self):
        sql = """CREATE TABLE lifeperiods_dyadicaggressions_el (
            ego VARCHAR, 
            period varchar, 
            period_start DATE, 
            period_end DATE, 
            H1 varchar, 
            H2 varchar, 
            weight FLOAT, 
            in_group varchar,
            context varchar);"""
        return(sql)
        
    def insert_records_sql(self):
        sql="""INSERT INTO 'lifeperiods_dyadicaggressions_el' 
                (ego, period, period_start, period_end, H1, H2, weight, in_group, context)
                SELECT 
                 ego, period, period_start, period_end, H1, H2, weight, in_group, context
                FROM lifeperiods_aggressions_el
                WHERE lifeperiods_aggressions_el.in_group = 'N';"""
        return(sql)
        
    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        index_sql = behavior_index_ddl(self.tablename)
        return([index_sql])
                

class AIPairsMaker(TableMaker):
    """create lifeperiods_ai_allpairs table"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        self.tablename = 'lifeperiods_ai_allpairs'
                    
    def make_table_sql(self):
        
        sql = """CREATE TABLE lifeperiods_ai_allpairs (ego VARCHAR, period varchar, 
                period_start DATE, period_end DATE, H1 varchar, H2 varchar, 
                H1_minutes INT, H2_minutes INT,
                H1_sessions INT, H2_sessions INT);"""
        return(sql)
        
    def insert_records_sql(self):
        """ get container for ais for both session counting and minutes summing"""
        #   make unique pairs of animals by alphabetizing with CASE statements
        #  get minutes seen for each animal, but use same alphabetizing scheme to keep it straight

        sql="""INSERT INTO lifeperiods_ai_allpairs 
           (ego, period, period_start, period_end, H1, H2, 
            H1_minutes, H2_minutes, H1_sessions, H2_sessions)
        SELECT
            CL1.ego as ego,
            CL1.period as period,
            CL1.period_start as period_start,
            CL1.period_end as period_end,
            CASE WHEN 
                CL1.hyena < CL2.hyena THEN CL1.hyena ELSE CL2.hyena 
                END AS H1,
            CASE WHEN 
                CL1.hyena < CL2.hyena THEN CL2.hyena ELSE CL1.hyena
                END AS H2, 
            CASE WHEN 
                CL1.hyena < CL2.hyena THEN CL1.minutes_seen ELSE CL2.minutes_seen 
                END AS H1_minutes,
            CASE WHEN 
                CL1.hyena < CL2.hyena THEN CL2.minutes_seen ELSE CL1.minutes_seen 
                END AS H2_minutes,
            CASE WHEN 
                CL1.hyena < CL2.hyena THEN CL1.sessions_seen ELSE CL2.sessions_seen 
                END AS H1_sessions,
            CASE WHEN 
                CL1.hyena < CL2.hyena THEN CL2.sessions_seen ELSE CL1.sessions_seen 
                END AS H2_sessions

            FROM
                lifeperiods_clanlists as CL1 inner join lifeperiods_clanlists as CL2 
            ON CL1.period = CL2.period and CL1.ego = CL2.ego
            WHERE H1 <> H2            
            GROUP BY CL1.ego, CL1.period, H1,H2;"""
            
            # additional criteria for testing AND CL1.ego < 'i'
        return(sql)

class AItogethersessionsMaker(TableMaker):
    """create  ai_togethersessions table"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        self.tablename = 'ai_togethersessions'
        self.table_sql = """CREATE TABLE 'ai_togethersessions' (ego varchar, period varchar, pstart date, pend date, H1 varchar, H2 varchar, sessions_together INTEGER, minutes_together INTEGER); """
        self.insert_sql="""INSERT INTO 
        ai_togethersessions (ego, period, pstart, pend, H1, H2, sessions_together, minutes_together )
            SELECT 
                lifeperiods_ai_allpairs.ego as ego, 
                lifeperiods_ai_allpairs.period as period,
                lifeperiods_ai_allpairs.period_start as pstart, 
                lifeperiods_ai_allpairs.period_end as pend,
                H1,H2, 
                count(sessions_plus.session) as sessions_together,
                sum(sessions_plus.sessionminutes) as minutes_together
                
            from
                lifeperiods_ai_allpairs, sessions_plus
            where sessions_plus.sessiondate
                  between lifeperiods_ai_allpairs.period_start and lifeperiods_ai_allpairs.period_end
                  and sessions_plus.hyenas like  "%," || H1 || ",%"
                  and sessions_plus.hyenas like  "%," || H2 || ",%"
            group by
                lifeperiods_ai_allpairs.ego, lifeperiods_ai_allpairs.period, H1,H2;"""

        
    def make_table_sql(self):
        return(self.table_sql)
        
    def insert_records_sql(self):
        return(self.insert_sql)

        def index_sql_array(self):
            return( ["""CREATE UNIQUE INDEX idx_ai_togethersessions  ON ai_togethersessions (ego,period,H1,H2);""", 
                     """CREATE INDEX idx_ai_togethersessions_H1      ON ai_togethersessions (H1);""", 
                     """CREATE INDEX ai_togethersessions_period      ON ai_togethersessions (period);""",
                     """CREATE INDEX idx_ai_togethersessions_ego     ON ai_togethersessions (ego);"""
                    ])


class AisElMaker(TableMaker):
    """create  lifeperiods_ais_el table"""
    def __init__(self, db):
        TableMaker.__init__(self,db)
        
        self.tablename = 'lifeperiods_ais_el'
        self.table_sql = """CREATE TABLE lifeperiods_ais_el as
                SELECT ai_togethersessions.ego as ego,
                ai_togethersessions.period as period,
                ai_togethersessions.H1 as H1,
                ai_togethersessions.H2 as H2,
                cast( ai_togethersessions.sessions_together as float) /
                    ( lifeperiods_ai_allpairs.H1_sessions
                    + lifeperiods_ai_allpairs.H2_sessions
                    - ai_togethersessions.sessions_together )
                 as ai_sessions, 
                cast( ai_togethersessions.minutes_together as float) /
                    ( lifeperiods_ai_allpairs.H1_minutes
                    + lifeperiods_ai_allpairs.H2_minutes
                    - ai_togethersessions.minutes_together )
                 as ai_minutes, 
                 0.0 as weight 

                FROM ai_togethersessions
                INNER JOIN lifeperiods_ai_allpairs
                on  ai_togethersessions.ego    = lifeperiods_ai_allpairs.ego
                and ai_togethersessions.period = lifeperiods_ai_allpairs.period
                and ai_togethersessions.h1     = lifeperiods_ai_allpairs.H1
                and ai_togethersessions.h2     = lifeperiods_ai_allpairs.H2;"""

    def make_table_sql(self):
        return(self.table_sql)

    def bonus_sql(self):
        """select ai to put into edge list weight column"""
        sql = "update lifeperiods_ais_el set weight=ai_sessions"
        return(sql)
        
    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        return([behavior_index_ddl(self.tablename)])
        
####### NORMALIZE THE WEIGHTS IN EDGELISTS AFTER THE FACT
## called by main after db is built..

def normalize_edge_weights(db):
    """calc all weights to be weight/ai for behavior-based edge lists"""
    # assumes current edge weights in tables are raw counts.  
    # TO DO: create raw_weight column to hold raw counts/averages
    
    # because tables are named consistently, can use a template and interate behavior names
    # also the ordering of names in AI table may not be the same as in behavior tables
    # so need to check both h1 =h1... or h2 = h1.  Probably a better way to do this
    # t1.H1||t1.H2 = t2.H1||t2.H2 or t1.H2,t1.H1) = concat(t2.H1,t2.H2) 
    sqltemplate = """UPDATE lifeperiods_{0}_el
            set weight = (
                select round(lifeperiods_{0}_el.weight/lifeperiods_ais_el.weight,4) 
                from   lifeperiods_ais_el 
                where  lifeperiods_{0}_el.ego = lifeperiods_ais_el.ego 
                and    lifeperiods_{0}_el.period = lifeperiods_ais_el.period
                and (  ( lifeperiods_{0}_el.H1 = lifeperiods_ais_el.H1 and 
                         lifeperiods_{0}_el.H2 = lifeperiods_ais_el.H2 ) 
                    or ( lifeperiods_{0}_el.H2 = lifeperiods_ais_el.H1 and 
                         lifeperiods_{0}_el.H1 = lifeperiods_ais_el.H2 )
                    )
                );"""
            
    for behavname in ["aggressions","greetings","liftlegboth","ll"]:
        sql = sqltemplate.format(behavname)
        db.execddl(sql)
    # end
   
# -------------------
####    file management for sqlite database files
#--------------------
   
def open_a_copy_db(startdbfile,workingdbfile):    
    # try...
    # erases workingdbfile... confirm if present?
    shutil.copyfile(startdbfile,workingdbfile)  
    db = SqliteDB(workingdbfile)
    return(db)


def valid_file(filepath):
    if not os.path.exists(filepath):
        msg = "can't find file %s" % filepath
        raise argparse.ArgumentTypeError(msg)
    return filepath

        
if __name__ == "__main__":
    
    if not sqlitedb.newer_sqlite_version('3.8'):
        raise RuntimeError("requires newer sqlite3 version")
    
    # command line arguments....
    parser = argparse.ArgumentParser()
    parser.add_argument('fisibase',
        help='initial database with fisibase tables (required)',
        type=valid_file )        
    parser.add_argument('--dbname', '-d',
        required=False,
        help='base name of output database' )
    parser.add_argument('--start', '-s',
        required=False,
        default='a',
        help='starting letter for selecting individuals name' )
    parser.add_argument('--end', '-e',
        required=False,
        default='z',
        help='letter for selecting individuals name' )

    #todo: add V for verbose flag
       
    args = parser.parse_args()
    fisibasefile = args.fisibase
    start_alpha = args.start
    end_alpha   = args.end
        
    if args.dbname:
        base_file_name = args.dbname
    else:
        base_file_name = os.path.splitext(fisibasefile)[0]
    
    socialnetfile = '{0}.sna.{1}-{2}.sqlite'.format(base_file_name,start_alpha,end_alpha)

    # feedback...
    print('building db {0} from {1}'.format(socialnetfile,fisibasefile))
     
    db = open_a_copy_db(fisibasefile, socialnetfile )
    
    # try     
    # build_partial_egonames(db,start_alpha,end_alpha )
    
    # and then try         
    build_db(db)
    #
    # feedback output
    print('---- resulting tables in {0} ----'.format(db.dbfile))
    print(db.tables())
    
    # clean up
    db.close()
    