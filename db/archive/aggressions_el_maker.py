"""This is test code to try the factor pattern for creating builders with TableMaker class.
The single method creates aggressions table builder based table maker class.
Usage agg_maker = agg_maker_factory(db); result = agg_maker.make_table()"""

from tablemaker import TableMaker
import * from  builder_utils

# TODO check dependencies format
def agg_maker_factory(db):
    """ creates aggressions table maker object that can be used in database builders"""

    agg_maker = TableMaker(db, required_tables=["lifeperiods","aggressions","ais"], tablename = 'lifeperiods_aggressions_el')

    agg_maker.table_sql = """CREATE TABLE lifeperiods_aggressions_el (
            ego VARCHAR,
            period VARCHAR,
            period_start DATE,
            period_end DATE,
            H1 VARCHAR,
            H2 VARCHAR,
            weight FLOAT,
            in_group VARCHAR,
            context VARCHAR);"""

    agg_maker.insert_sql = """INSERT INTO 'lifeperiods_aggressions_el'
                (ego, period, period_start, period_end, H1, H2, weight,in_group,context)
                SELECT
                 lifeperiods.ego AS ego,
                 lifeperiods.period AS period,
                 lifeperiods.period_start AS period_start,
                 lifeperiods.period_end AS period_end,
                 aggressions.aggressor AS H1,
                 aggressions.recip AS H2,
                 avg(aggressions.behavior1) AS weight,
                 CASE
                     WHEN aggressions.isgroup = 'Y'
                     THEN 'Y'ELSE 'N' END
                         AS in_group,
                 aggressions.context AS context
            FROM aggressions, lifeperiods
            WHERE
                    aggressions.sessiondate IS NOT NULL
                AND aggressions.sessiondate BETWEEN lifeperiods.period_start AND lifeperiods.period_end
                AND aggressions.aggressor IN (SELECT lifeperiods_clanlists.hyena FROM
                      lifeperiods_clanlists
                      WHERE lifeperiods.period =  lifeperiods_clanlists.period
                      AND lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
                AND aggressions.recip     IN (SELECT lifeperiods_clanlists.hyena FROM
                      lifeperiods_clanlists
                      WHERE lifeperiods.period =  lifeperiods_clanlists.period
                      AND lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
            GROUP BY
                lifeperiods.ego,lifeperiods.period, lifeperiods.period_start,
                H1,H2;"""


    agg_maker.index_sql = behavior_index_ddl(self.tablename)
    agg_maker.bonus_sql_str = normalize_edge_weights_sql(self.tablename)

    return(agg_maker)




