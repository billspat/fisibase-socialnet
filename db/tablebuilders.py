""" socialnetwork database project, singleton classes for generating and running sql to build each table
    indicating tabls dependencies.   These classes can be run individually, but the snabuilder.py program,
    has an array of them and each run in turn to build the database one table at a time"""

from builder_utils import normalize_edge_weights_sql, behavior_index_ddl
from sqlitedb import SqliteDB
from tablemaker import TableMaker


class AggressionsElMaker(TableMaker):
    """create aggressions_el table"""

    def __init__(self, db):
        TableMaker.__init__(self, db)
        self.tablename = 'lifeperiods_aggressions_el'

    def make_table_sql(self):
        sql = """CREATE TABLE lifeperiods_aggressions_el (
            ego VARCHAR,
            period VARCHAR,
            period_start DATE,
            period_end DATE,
            H1 VARCHAR,
            H2 VARCHAR,
            weight FLOAT,
            in_group VARCHAR,
            context VARCHAR);"""
        return (sql)

    def insert_records_sql(self):
        sql = """INSERT INTO 'lifeperiods_aggressions_el'
                (ego, period, period_start, period_end, H1, H2, weight,in_group,context)
                SELECT
                 lifeperiods.ego AS ego,
                 lifeperiods.period AS period,
                 lifeperiods.period_start AS period_start,
                 lifeperiods.period_end AS period_end,
                 aggressions.aggressor AS H1,
                 aggressions.recip AS H2,
                 avg(aggressions.behavior1) AS weight,
                 CASE
                     WHEN aggressions.isgroup = 'Y'
                     THEN 'Y'ELSE 'N' END
                         AS in_group,
                 aggressions.context AS context
            FROM aggressions, lifeperiods
            WHERE
                    aggressions.sessiondate IS NOT NULL
                AND aggressions.sessiondate BETWEEN lifeperiods.period_start AND lifeperiods.period_end
                AND aggressions.aggressor IN (SELECT lifeperiods_clanlists.hyena FROM
                      lifeperiods_clanlists
                      WHERE lifeperiods.period =  lifeperiods_clanlists.period
                      AND lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
                AND aggressions.recip     IN (SELECT lifeperiods_clanlists.hyena FROM
                      lifeperiods_clanlists
                      WHERE lifeperiods.period =  lifeperiods_clanlists.period
                      AND lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
            GROUP BY
                lifeperiods.ego,lifeperiods.period, lifeperiods.period_start,
                H1,H2;"""
        return (sql)

    def bonus_sql(self):
        """ add AIs weight normalizing"""
        # check that AIs table exists
        return (normalize_edge_weights_sql(self.tablename))

    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        index_sql = behavior_index_ddl(self.tablename)
        return ([index_sql])


class AItogethersessionsMaker(TableMaker):
    """create  ai_togethersessions table"""

    def __init__(self, db):
        TableMaker.__init__(self, db)
        self.tablename = 'ai_togethersessions'
        self.table_sql = """CREATE TABLE 'ai_togethersessions' (ego VARCHAR, period VARCHAR, pstart DATE, pend DATE, H1 VARCHAR, H2 VARCHAR, sessions_together INTEGER, minutes_together INTEGER); """
        self.insert_sql = """INSERT INTO
        ai_togethersessions (ego, period, pstart, pend, H1, H2, sessions_together, minutes_together )
            SELECT
                lifeperiods_ai_allpairs.ego AS ego,
                lifeperiods_ai_allpairs.period AS period,
                lifeperiods_ai_allpairs.period_start AS pstart,
                lifeperiods_ai_allpairs.period_end AS pend,
                H1,H2,
                count(sessions_plus.session) AS sessions_together,
                sum(sessions_plus.sessionminutes) AS minutes_together
            FROM
                lifeperiods_ai_allpairs, sessions_plus
            WHERE sessions_plus.sessiondate
                  BETWEEN lifeperiods_ai_allpairs.period_start AND lifeperiods_ai_allpairs.period_end
                  AND sessions_plus.hyenas LIKE  "%," || H1 || ",%"
                  AND sessions_plus.hyenas LIKE  "%," || H2 || ",%"
            GROUP BY
                lifeperiods_ai_allpairs.ego, lifeperiods_ai_allpairs.period, H1,H2;"""
        self.index_sql = ["""CREATE UNIQUE INDEX idx_ai_togethersessions  ON ai_togethersessions (ego,period,H1,H2);""",
                          """CREATE INDEX idx_ai_togethersessions_H1      ON ai_togethersessions (H1);""",
                          """CREATE INDEX ai_togethersessions_period      ON ai_togethersessions (period);""",
                          """CREATE INDEX idx_ai_togethersessions_ego     ON ai_togethersessions (ego);"""
                         ]

    def make_table_sql(self):
        """return sql to create table defined in init"""
        return (self.table_sql)

    def insert_records_sql(self):
        """return sql to insert records defined in init """
        return (self.insert_sql)

    def index_sql_array(self):
        """defined in init"""
        return (self.index_sql)


class AisElMaker(TableMaker):
    """create  lifeperiods_ais_el table"""

    def __init__(self, db):
        TableMaker.__init__(self, db)

        self.tablename = 'lifeperiods_ais_el'
        self.table_sql = """CREATE TABLE lifeperiods_ais_el AS
                SELECT ai_togethersessions.ego AS ego,
                ai_togethersessions.period AS period,
                ai_togethersessions.H1 AS H1,
                ai_togethersessions.H2 AS H2,
                cast( ai_togethersessions.sessions_together AS FLOAT) /
                    ( lifeperiods_ai_allpairs.H1_sessions
                    + lifeperiods_ai_allpairs.H2_sessions
                    - ai_togethersessions.sessions_together )
                 AS ai_sessions,
                cast( ai_togethersessions.minutes_together AS FLOAT) /
                    ( lifeperiods_ai_allpairs.H1_minutes
                    + lifeperiods_ai_allpairs.H2_minutes
                    - ai_togethersessions.minutes_together )
                 AS ai_minutes,
                 0.0 AS weight

                FROM ai_togethersessions
                INNER JOIN lifeperiods_ai_allpairs
                ON  ai_togethersessions.ego    = lifeperiods_ai_allpairs.ego
                AND ai_togethersessions.period = lifeperiods_ai_allpairs.period
                AND ai_togethersessions.h1     = lifeperiods_ai_allpairs.H1
                AND ai_togethersessions.h2     = lifeperiods_ai_allpairs.H2;"""

    def make_table_sql(self):
        return (self.table_sql)

    def bonus_sql(self):
        """select ai to put into edge list weight column"""
        sql = "UPDATE lifeperiods_ais_el SET weight=ai_sessions"
        return (sql)

    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        return ([behavior_index_ddl(self.tablename)])


class AIPairsMaker(TableMaker):
    """create lifeperiods_ai_allpairs table"""

    def __init__(self, db):
        TableMaker.__init__(self, db)
        self.tablename = 'lifeperiods_ai_allpairs'

    def make_table_sql(self):
        sql = """CREATE TABLE lifeperiods_ai_allpairs (ego VARCHAR, period VARCHAR,
                period_start DATE, period_end DATE, H1 VARCHAR, H2 VARCHAR,
                H1_minutes INT, H2_minutes INT,
                H1_sessions INT, H2_sessions INT);"""
        return (sql)

    def insert_records_sql(self):
        """ get container for ais for both session counting and minutes summing"""
        #   make unique pairs of animals by alphabetizing with CASE statements
        # get minutes seen for each animal, but use same alphabetizing scheme
        # to keep it straight

        sql = """INSERT INTO lifeperiods_ai_allpairs
           (ego, period, period_start, period_end, H1, H2,
            H1_minutes, H2_minutes, H1_sessions, H2_sessions)
        SELECT
            CL1.ego AS ego,
            CL1.period AS period,
            CL1.period_start AS period_start,
            CL1.period_end AS period_end,
            CASE WHEN
                CL1.hyena < CL2.hyena THEN CL1.hyena ELSE CL2.hyena
                END AS H1,
            CASE WHEN
                CL1.hyena < CL2.hyena THEN CL2.hyena ELSE CL1.hyena
                END AS H2,
            CASE WHEN
                CL1.hyena < CL2.hyena THEN CL1.minutes_seen ELSE CL2.minutes_seen
                END AS H1_minutes,
            CASE WHEN
                CL1.hyena < CL2.hyena THEN CL2.minutes_seen ELSE CL1.minutes_seen
                END AS H2_minutes,
            CASE WHEN
                CL1.hyena < CL2.hyena THEN CL1.sessions_seen ELSE CL2.sessions_seen
                END AS H1_sessions,
            CASE WHEN
                CL1.hyena < CL2.hyena THEN CL2.sessions_seen ELSE CL1.sessions_seen
                END AS H2_sessions

            FROM
                lifeperiods_clanlists AS CL1 INNER JOIN lifeperiods_clanlists AS CL2
            ON CL1.period = CL2.period AND CL1.ego = CL2.ego
            WHERE H1 <> H2
            GROUP BY CL1.ego, CL1.period, H1,H2;"""

        # TODO additional criteria for testing AND CL1.ego < 'i'
        return (sql)


class ClanListsMaker(TableMaker):
    """create  lifeperiods_clanlists table"""

    def __init__(self, db, clanlist_seen_threshold=10):
        TableMaker.__init__(self, db, tablename='lifeperiods_clanlists')
        self.clanlist_seen_threshold = clanlist_seen_threshold
        self.tablename = 'lifeperiods_clanlists'

    def make_table_sql(self):
        # change hyenas.clan like 'talek%' to hyenas.clan like
        # concat(egos.clan, '%')
        self.table_sql = """CREATE TABLE lifeperiods_clanlists AS
            SELECT distinct
                  lifeperiods.ego          as ego,
                  lifeperiods.period       as period,
                  lifeperiods.period_start as period_start,
                  lifeperiods.period_end   as period_end,
                  hyenaspersession.hyena   as hyena,
                  0                        as hyenarank,
                  hyenas.sex               as sex,
                  (julianday(lifeperiods.period_start)-julianday(hyenas.birthdate))
                                           as hyenaAgeAtStart,
                  count(sessions_plus.session)
                                           as sessions_seen,
                  sum((strftime('%s',sessions_plus.stop)-strftime('%s',sessions_plus.start) ) /60.0)
                                           as minutes_seen
            FROM (hyenaspersession
                  inner join sessions_plus on hyenaspersession.session = sessions_plus.session)
                  inner join hyenas on hyenaspersession.hyena = hyenas.hyena,
                  lifeperiods
            WHERE
                  SESSIONDATE between lifeperiods.period_start and lifeperiods.period_end
                  and hyenas.clan like 'talek%'
            GROUP BY
                lifeperiods.ego, lifeperiods.period, hyenaspersession.hyena
            HAVING
                sessions_seen >= {0};""".format(self.clanlist_seen_threshold)
        return (self.table_sql)

    def index_sql_array(self):
        return (["""CREATE UNIQUE INDEX idx_lifeperiods_clanlists
                    ON lifeperiods_clanlists (ego,period,hyena);""",
                 """CREATE INDEX idx_lifeperiods_clanlists_dates
                     ON lifeperiods_clanlists (period_start,period_end,hyena);""",
                 """CREATE INDEX idx_lifeperiods_clanlists_period
                     ON lifeperiods_clanlists (period);""",
                 """CREATE INDEX idx_lifeperiods_clanlists_ego
                     ON lifeperiods_clanlists (ego);"""
                ])

    def bonus_sql(self):
        # now that clanlists are done, can count them and update clansize...
        calc_clansize_sql = """UPDATE lifeperiods SET clan_size =
         (SELECT count(lifeperiods_clanlists.hyena) AS clan_size FROM lifeperiods_clanlists
         WHERE lifeperiods.ego = lifeperiods_clanlists.ego AND  lifeperiods.period = lifeperiods_clanlists.period
         GROUP BY lifeperiods_clanlists.ego, lifeperiods_clanlists.period); """
        return (calc_clansize_sql)


class DyadicAggressionsElMaker(TableMaker):
    """create dyadic aggressions_el table"""

    def __init__(self, db):
        TableMaker.__init__(self, db)
        self.tablename = 'lifeperiods_dyadicaggressions_el'

    def make_table_sql(self):
        sql = """CREATE TABLE lifeperiods_dyadicaggressions_el (
            ego VARCHAR,
            period VARCHAR,
            period_start DATE,
            period_end DATE,
            H1 VARCHAR,
            H2 VARCHAR,
            weight FLOAT,
            in_group VARCHAR,
            context VARCHAR);"""
        return (sql)

    def insert_records_sql(self):
        sql = """INSERT INTO 'lifeperiods_dyadicaggressions_el'
                (ego, period, period_start, period_end, H1, H2, weight, in_group, context)
                SELECT
                 ego, period, period_start, period_end, H1, H2, weight, in_group, context
                FROM lifeperiods_aggressions_el
                WHERE lifeperiods_aggressions_el.in_group = 'N';"""
        return (sql)

    def bonus_sql(self):
        """ add AIs weight normalizing"""
        # check that AIs table exists
        return (normalize_edge_weights_sql(self.tablename))

    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        index_sql = behavior_index_ddl(self.tablename)
        return ([index_sql])


class EgonamesMaker(TableMaker):
    """ names of animals that meet inclusion criteria, for the specified clan"""

    def __init__(self, db, max_session_date, select_hyenas_sql, clan="talek"):
        TableMaker.__init__(self, db)
        self.tablename = 'egonames'
        self.max_session_date = max_session_date

        self.table_sql = """CREATE TABLE egonames (hyena VARCHAR, PRIMARY KEY(hyena));"""

        if (select_hyenas_sql): 
            # use parameter
            self.select_hyenas_sql = select_hyenas_sql
        else:
            # default, which is ALSO set in snabuilder, AND usually overridden by studies
            # potentially remove this redundant code, or keep it so it can be run stand alone?
            self.select_hyenas_sql = """SELECT hyenas.hyena as hyena
            FROM hyenas
            WHERE   hyenas.birthdate is not null
                and hyenas.mom       is not null
                and hyenas.firstseen is not null
                and hyenas.dengrad   is not null
                and julianday(hyenas.firstseen) < julianday(date(hyenas.birthdate, "+90 days"))
                and strftime('%Y', hyenas.firstseen) >= '1989' and hyenas.clan='{0}'
                and julianday(hyenas.firstseen) < julianday('{1}')
            order by  hyenas.birthdate;
            """.format(clan, max_session_date)
            
        # build the actualy insert statment    
        # the select hyenas sql is seperated from this so that it's easier to test
        self.insert_sql = "INSERT INTO egonames (hyena) {0}".format(
            self.select_hyenas_sql)

    def make_table_sql(self):
        return (self.table_sql)

    def insert_records_sql(self):
        return (self.insert_sql)


class EgosTableMaker(TableMaker):
    """create  egos table based on egonames table and max session date parameter. 
    requires a max_session_data parameter or else will crash """

    def __init__(self, db, max_session_date):
        # requires egonames table
        TableMaker.__init__(self, db, ('egonames'))

        self.tablename = 'egos'
        self.max_session_date = max_session_date
        # need a temp table with ego and data in it to do the update becuase using agregate function... and this is weird to inject two table creations into one here
        # but this should work

        #	self.db.execddl("DROP TABLE IF EXISTS age_last_seen;")
        #	self.db.execddl(age_last_seen_table_sql)

        self.make_sql = """
            CREATE TABLE egos (
               ego               VARCHAR PRIMARY KEY,
               sex               VARCHAR,  -- from hyenas table
               mom               VARCHAR,  -- from hyenas table
               birthdate         DATE,
               firstseen         DATE,
               disappeared       DATE,
               lastsession       DATE,
               dengrad           DATE,
               fate              VARCHAR,
               clan              VARCHAR,
               longevity_days    INTEGER,
               age_at_last_seen  REAL,
               annual_rs         REAL,
               age_at_dispersal  REAL
            ); """

        self.select_sql = """ SELECT
                hyenas.hyena AS ego,
                hyenas.sex,
                hyenas.mom,
                hyenas.birthdate,
                hyenas.firstseen,
                hyenas.disappeared,
                age_last_seen.lastsession,
                hyenas.dengrad,
                hyenas.fate,
                hyenas.clan,
                ( julianday(hyenas.disappeared) - julianday(hyenas.birthdate) ) AS longevity_days,
                age_last_seen.age_at_last_seen AS age_at_last_seen,
                CASE
                    WHEN hyenas.sex LIKE 'm' AND
                         hyenas.disappeared IS NOT NULL AND
                         hyenas.fate LIKE 'd'
                         AND hyenas.disappeared < '{0}'
                    THEN  age_last_seen.age_at_last_seen ELSE NULL END
                    AS age_at_dispersal
            FROM age_last_seen INNER JOIN hyenas  
                 ON age_last_seen.hyena = hyenas.hyena;
            """.format(self.max_session_date)
             # SEE BELOW FOR age_last_seen; includes all egonames, so don't need to link on egonames
             
        self.insert_sql = """INSERT INTO egos (
                ego,
                sex,
                mom,
                birthdate,
                firstseen,
                disappeared,
                lastsession,
                dengrad,
                fate,
                clan,
                longevity_days,
                age_at_last_seen,
                age_at_dispersal )
                {0}""".format(self.select_sql)

    def make_table_sql(self):
        return (self.make_sql)

    def insert_records_sql(self):
        """ build full egos table from hyenas and AgeLastSeen table temp.
            Ignore hyenas.disappeared data after last sessions_plus.sessiondate"""

        self.db.execsql("DROP TABLE IF EXISTS age_last_seen;    ")

        self.age_last_seen_table_sql = """CREATE TEMP TABLE age_last_seen AS
             SELECT 
               hyenas.hyena AS hyena,
               
               CASE 
               WHEN hyenas.disappeared is null THEN
                     round((julianday('{0}')- julianday(hyenas.birthdate))/365.0,2) 
               WHEN julianday(hyenas.disappeared) > julianday('{0}') THEN
                     round((julianday('{0}')- julianday(hyenas.birthdate))/365.0,2)
               ELSE 
                     round((julianday(hyenas.disappeared) - julianday(hyenas.birthdate))/365.0,2) 
               END 
                   AS age_at_last_seen,
                   
               max(sessions_plus.sessiondate) AS lastsession
             FROM sessions_plus INNER JOIN hyenaspersession ON sessions_plus.session = hyenaspersession.session
               INNER JOIN hyenas   ON hyenas.hyena = hyenaspersession.hyena
               INNER JOIN egonames ON hyenas.hyena = egonames.hyena
             GROUP BY hyenas.hyena;
             """.format(self.max_session_date)

        self.db.execsql(self.age_last_seen_table_sql)
        return (self.insert_sql)

    # CASE WHEN
    #     hyenas.sex like 'm' and
    #     hyenas.disappeared is not null and
    #     hyenas.fate like 'd'
    #     and hyenas.disappeared < max(sessions_plus.sessiondate)
    # THEN
    #     age_last_seen.age_at_last_seen
    # ELSE
    #     NULL
    # END
    #     as age_at_dispersal

    def bonus_sql(self):
        """calculate a reproductive success values for females, and update rs_days column.
            Requires temporary table to build these values up, and then copied into the db """

        repro_success_sql = """CREATE TEMP TABLE repro_success AS SELECT moms.id AS hyena,
            count(cubs.id) AS cubcount,
            moms.birthdate, min(cubs.birthdate) AS firstlitter,
            max(cubs.birthdate) AS lastlitter, moms.disappeared,
            round ( CASE
       WHEN moms.disappeared IS NULL THEN
           -- alive, use recent date
           count(cubs.id)/((julianday("2015-04-01") - julianday( min(cubs.birthdate)) )/365)
       WHEN julianday(max(cubs.birthdate)) - julianday(moms.disappeared) < 365 THEN
           -- died < 1 year soon after last litter, normalize to litter + 1 year)
           count(cubs.id)/((julianday(max(cubs.birthdate))+365 - julianday( min(cubs.birthdate)) )/365)
       ELSE
           -- lived past litter + 10 months, use actual disappear date
           count(cubs.id)/((julianday(moms.disappeared) - julianday( min(cubs.birthdate)) )/365)
       END,3) AS annual_repro_sucess
       FROM hyenas  AS cubs INNER JOIN hyenas AS moms ON cubs.mom = moms.id
       WHERE moms.birthdate IS NOT NULL AND moms.clan = 'talek' AND moms.id IN (SELECT ego FROM egos)
            AND moms.id != 'hk' AND moms.id != 'cen'
       GROUP BY moms.id"""
        # end of repro_success_sql

        self.db.execddl("DROP TABLE IF EXISTS repro_success;")
        self.db.execddl(repro_success_sql)

        update_repro_success_sql = """UPDATE egos SET
         annual_rs = ( SELECT repro_success.annual_repro_sucess
            FROM repro_success
                     WHERE repro_success.hyena = egos.ego);"""

        return (update_repro_success_sql)


class GreetingsElMaker(TableMaker):
    """create  greetings_el table"""

    def __init__(self, db):
        TableMaker.__init__(self, db)
        self.tablename = 'lifeperiods_greetings_el'
        self.alt_table_sql = """CREATE TABLE lifeperiods_greetings_el (ego TEXT, period TEXT, period_start NUM, period_end NUM, H1, H2, weight, greet_role VARCHAR);"""

        self.table_sql = """CREATE TABLE  'lifeperiods_greetings_el'  AS
            SELECT
                lifeperiods.ego AS ego,
                lifeperiods.period AS period,
                lifeperiods.period_start AS period_start,
                lifeperiods.period_end AS period_end,
                CASE WHEN greetings.ID1 < greetings.ID2 THEN greetings.ID1 ELSE greetings.ID2 END AS H1,
                CASE WHEN greetings.ID1 < greetings.ID2 THEN greetings.ID2 ELSE ID1 END AS H2,
                count(*) AS weight,
                '' AS greet_role
            FROM greetings,lifeperiods
            WHERE
                 greetings.sessiondate BETWEEN lifeperiods.period_start AND lifeperiods.period_end
                 AND greetings.ID1 IS NOT NULL AND greetings.ID2 IS NOT NULL
                 AND GRTOccured = "y"
                 AND greetings.ID1 IN (SELECT lifeperiods_clanlists.hyena FROM
                      lifeperiods_clanlists
                      WHERE lifeperiods.period =  lifeperiods_clanlists.period
                      AND lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
                 AND greetings.ID2 IN (SELECT lifeperiods_clanlists.hyena FROM
                      lifeperiods_clanlists
                      WHERE lifeperiods.period =  lifeperiods_clanlists.period
                      AND lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
            GROUP BY  lifeperiods.ego,lifeperiods.period, lifeperiods.period_start,H1,H2;
            """

    def make_table_sql(self):
        return (self.table_sql)

    def bonus_sql(self):
        """ add AIs weight normalizing"""
        # check that AIs table exists
        return (normalize_edge_weights_sql(self.tablename))

    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        index_sql = behavior_index_ddl(self.tablename)
        return ([index_sql])


class LiftlegElMaker(TableMaker):
    """create  ll_el table - seperate create table and inserts"""

    def __init__(self, db):
        TableMaker.__init__(self, db)
        self.tablename = 'lifeperiods_ll_el'

    def make_table_sql(self):
        sql = """CREATE TABLE lifeperiods_ll_el (ego VARCHAR, period VARCHAR, period_start DATE,
                period_end DATE, H1 VARCHAR, H2 VARCHAR, weight INT, greet_role VARCHAR);"""
        return (sql)

    def insert_records_sql(self):
        sql = """INSERT INTO 'lifeperiods_ll_el'
            (ego, period, period_start, period_end, H1, H2, weight, greet_role)
        SELECT
             lifeperiods.ego AS ego,
             lifeperiods.period AS period,
             lifeperiods.period_start AS period_start,
             lifeperiods.period_end AS period_end,
             greetings.ll_solicitor AS H1,
             greetings.ll_reciever AS H2,
             count(*) AS weight,
             ll_symmetry
        FROM greetings, lifeperiods
        WHERE
             greetings.sessiondate BETWEEN lifeperiods.period_start AND lifeperiods.period_end
             AND H1 IS NOT NULL AND H2 IS NOT NULL
             AND greetings.ll_solicitor IN (SELECT lifeperiods_clanlists.hyena FROM
                      lifeperiods_clanlists
                      WHERE lifeperiods.period =  lifeperiods_clanlists.period
                      AND lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
             AND greetings.ll_reciever  IN (SELECT lifeperiods_clanlists.hyena FROM
                      lifeperiods_clanlists
                      WHERE lifeperiods.period =  lifeperiods_clanlists.period
                      AND lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
        GROUP BY  lifeperiods.ego, lifeperiods.period, lifeperiods.period_start,H1,H2;"""
        return (sql)

    def bonus_sql(self):
        """ add AIs weight normalizing"""
        # check that AIs table exists
        return (normalize_edge_weights_sql(self.tablename))

    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        index_sql = behavior_index_ddl(self.tablename)
        return ([index_sql])

        # FILTER BY ONLY ANIMALS IN THE CLAN LIST FOR THAT PERIOD

        # add column for 'edge type'  = igraph edge attribute = solictor when H1 is solictor
        # second pass where only greetings.ll_symmetry = 'both' and
        # greetings.ll_reciever as H1,
        # greetings.ll_solicitor as H2,
        # "reciprocator" as greet_role


class LiftlegbothElMaker(TableMaker):
    """create lifeperiods_liftlegboth_el table"""

    def __init__(self, db):
        TableMaker.__init__(self, db)
        self.tablename = 'lifeperiods_liftlegboth_el'

    def make_table_sql(self):
        sql = """CREATE TABLE lifeperiods_liftlegboth_el (ego VARCHAR, period VARCHAR, period_start DATE,
                period_end DATE, H1 VARCHAR, H2 VARCHAR, weight INT, greet_role VARCHAR);"""
        return (sql)

    def insert_records_sql(self):
        sql = """INSERT INTO 'lifeperiods_liftlegboth_el'
            (ego, period, period_start, period_end, H1, H2, weight, greet_role)
        SELECT
             lifeperiods.ego AS ego,
             lifeperiods.period AS period,
             lifeperiods.period_start AS period_start,
             lifeperiods.period_end AS period_end,
             greetings.ll_solicitor AS H1,
             greetings.ll_reciever AS H2,
             count(*) AS weight,
             ' ' AS greet_role
        FROM greetings, lifeperiods
        WHERE
             greetings.sessiondate BETWEEN lifeperiods.period_start AND lifeperiods.period_end
             AND H1 IS NOT NULL AND H2 IS NOT NULL AND greetings.ll_symmetry = 'both'
             AND greetings.ll_solicitor IN (SELECT lifeperiods_clanlists.hyena FROM
                      lifeperiods_clanlists
                      WHERE lifeperiods.period =  lifeperiods_clanlists.period
                      AND lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
             AND greetings.ll_reciever  IN (SELECT lifeperiods_clanlists.hyena FROM
                      lifeperiods_clanlists
                      WHERE lifeperiods.period =  lifeperiods_clanlists.period
                      AND lifeperiods.ego =  lifeperiods_clanlists.ego
                 )
        GROUP BY  lifeperiods.ego, lifeperiods.period, lifeperiods.period_start,H1,H2;"""
        return (sql)

    def bonus_sql(self):
        """ add AIs weight normalizing"""
        # check that AIs table exists
        return (normalize_edge_weights_sql(self.tablename))

    def index_sql_array(self):
        """ sql code for indexes, if any, stored in an array/tuple"""
        index_sql = behavior_index_ddl(self.tablename)
        return ([index_sql])


# TODO: move extra data columns into new functions or files
# for example, prey availability column in it's own program
#

class PeriodsTableMaker(TableMaker):
    """create  Periods table, a list of the periods and a description
    This class requires an array of periods that has this structure:
    periods = [ {'seq': "1", 'name': 'cd', 'desc': 'dfs to dengrad'}, {etc}]
    The following class requires more fields that this..."""

    def __init__(self, db, periods_array):
        TableMaker.__init__(self, db)
        self.tablename = 'periods'
        self.periods = periods_array

    def make_table_sql(self):
        sql = """CREATE TABLE periods (
             seq INTEGER,
             period VARCHAR,
             period_desc VARCHAR,
             PRIMARY KEY(period) );"""
        return (sql)

    def insert_records_sql(self):
        values_template = "({0}, '{1}', '{2}')"
        values_array = [values_template.format(self.periods[i]["seq"],
                                               self.periods[i]["name"],
                                               self.periods[i]["desc"]) for i in range(0, len(self.periods))]

        insert_sql = """INSERT INTO periods (seq, period,period_desc) VALUES """
        insert_sql += ",".join(values_array)

        insert_sql += ";"
        return insert_sql


class LifePeriodsTableMaker(TableMaker):
    """create  LifePeriods table based on periods and db, create actual date ranges for each animal.
         Note this table has multiple UPDATE sql at the end based on those date ranges
         This class requires a periods array with the following structure:

         periods = [ {'seq': "1", 'name' : 'cd', 'desc': 'dfs to dengrad','start_expr' : "hyenas.firstseen",

                 'end_expr'   : "date(hyenas.dengrad, '-1 day')" ,'where'      :  None}
         """

    def __init__(self, db, periods,  max_session_date):
        TableMaker.__init__(self, db)
        self.db = db
        self.tablename = 'lifeperiods'
        # sql fragements used in union - no semicolons here
        self.periods = periods
        self.max_session_date = max_session_date
        
        self.table_sql = """CREATE TABLE lifeperiods (
             ego              VARCHAR,
             seq              INTEGER,
             period           VARCHAR,
             period_start     DATE,
             period_end       DATE,
             ego_period_rank  REAL,
             mom_period_rank  REAL,
             clan_size        INTEGER,
             sessions_count   INTEGER,
             sessions_alone   INTEGER,
             mom_alive        INTEGER,
             prey_level       INTEGER,
             PRIMARY KEY(ego,period)
         );
        """

    def make_insert_sql(self):
        """ this creates code to insert records into lifeperiods table; 
        for each item  in the periods array create sql and union them together"""
        template = """SELECT
               egos.ego,
               {seq}   AS seq,
              '{name}' AS period,
               {start_expr}  AS period_start,
               {end_expr}    AS period_end,
               0 AS sessions_count, 0 AS sessions_alone, NULL AS mom_alive, NULL AS prey_level
            FROM hyenas INNER JOIN egos ON hyenas.hyena = egos.ego"""

        # start the insert statement off
        insert_sql = """INSERT INTO lifeperiods (ego,seq, period,period_start,period_end,
                    sessions_count, sessions_alone, mom_alive,prey_level)"""
        
        # list comprehension to create sql statement for each item in periods array
        # use the template above.   Periods array items must have seq,start_expr and end_expr
        insert_sql = insert_sql + \
            "\nUNION \n".join([template.format(**self.periods[i])
                               for i in range(0, len(self.periods))])
        # add final semicolon and return
        insert_sql = insert_sql + ";"
        return (insert_sql)

    def make_table_sql(self):
        return (self.table_sql)

    def insert_records_sql(self):
        """ this unions the select statements defined in init above """
        return (self.make_insert_sql())

    def index_sql_array(self):
        return (
            ["CREATE INDEX idx_period_start ON lifeperiods  (period_start);"]
        )

    def bonus_sql(self):
        """bonus sql in tablemaker class is for single sql command. 
         the lifeperiods table needs so much 'clean up' that this code creates lots of sql
        then uses  db.exec command directly in this method to change the contents of the table
        as a side effect because there are three SQL DDL to call for this table.  
        The tablemaker class doesn't have a clean way of doing this, so run lots of sql here
        to run after the table is first created"""

        # -- mom's rank during the period added to this table for reference
        sql = """UPDATE lifeperiods
        SET mom_period_rank =
            (SELECT  ranks.stdrelativerank AS momrank
            FROM (ranks INNER JOIN egos ON ranks.hyena = egos.mom )
            WHERE lifeperiods.ego = egos.ego
              AND ranks.year <= strftime("%Y", period_end  )
              AND ranks.year >= strftime("%Y", period_start)
            GROUP BY ranks.hyena
        );
        """
        # exec sql to add moms' rank
        self.db.execddl(sql)

        # -- when animal is young does not have a rank, so insert the mom's rank
        sql = """UPDATE lifeperiods
            SET ego_period_rank = mom_period_rank
            WHERE ego_period_rank IS NULL;"""
        # exec sql to update table with mom's rank
        self.db.execddl(sql)

        # remove those egos that have one or more null ranks (e.g mom rank is null)
        # no rank means not part of the clan, so don't include them

        delete_sql = """DELETE FROM lifeperiods
            WHERE ego IN
                (SELECT DISTINCT ego
                    FROM lifeperiods
                    WHERE lifeperiods.mom_period_rank IS NULL
                );"""
        self.db.execddl(delete_sql)

        #remove all egos that ended up with periods past max_session_date
        # note that table hyenas is more up-to-date than many other tables. 
        # note we have to do this here, becuase only now do we know which lifeperiods 
        # extend past max_session_date
        egos_to_remove_sql = """select distinct ego 
                    from lifeperiods 
                    where date(lifeperiods.period_end) > date('{0}')
            """.format(self.max_session_date)
            
        # must first remove from egos table while  life periods table has these data
        self.db.execddl( "DELETE FROM egos WHERE ego IN ({0});".format(egos_to_remove_sql))
        # then can remove from lifeperiods table
        self.db.execddl( "DELETE FROM lifeperiods WHERE ego IN ({0});".format(egos_to_remove_sql))

                        
        # add 1/0 for mom alive field for each period
        mom_alive_sql = """UPDATE lifeperiods SET mom_alive =
            (SELECT CASE
                    WHEN moms.disappeared IS NULL THEN 1
                    WHEN julianday(moms.disappeared) >
                         julianday(lifeperiods.period_start) +
                         ( julianday(lifeperiods.period_end) -  julianday(lifeperiods.period_start)) / 2
                         THEN 1
                    ELSE 0
                    END AS mom_alive
            FROM hyenas INNER JOIN hyenas AS moms ON hyenas.mom = moms.hyena
            WHERE lifeperiods.ego = hyenas.hyena);
        """
        self.db.execddl(mom_alive_sql)

        # ADD COUNTING of hyenas in sessions here
        # count all sessions
        # session_alone_count = """update SELECT count(sessions_plus.session) as sessions_not_alone FROM lifeperiods, sessions where ego in session (?), unids is null, session_hyenas count = 1
        # """"

        # create temporary in-memory tables to hold calculations

        temp_session_count_sql = """CREATE TEMP TABLE session_counts AS
SELECT lifeperiods.ego AS ego, lifeperiods.period AS period, count(sessions_plus.session) AS session_count FROM (sessions_plus INNER JOIN hyenaspersession ON sessions_plus.session = hyenaspersession.session ) INNER JOIN lifeperiods ON lifeperiods.ego = hyenaspersession.hyena   WHERE sessions_plus.sessiondate >= lifeperiods.period_start AND sessions_plus.sessiondate <= lifeperiods.period_end
GROUP BY lifeperiods.ego, lifeperiods.period;"""

        temp_alone_session_count_sql = """CREATE TEMP TABLE alone_session_counts AS
        SELECT lifeperiods.ego AS ego, lifeperiods.period AS period, count(sessions_plus.session) AS alone_session_count FROM lifeperiods, sessions_plus
         WHERE sessions_plus.sessiondate >= lifeperiods.period_start
               AND sessions_plus.sessiondate <= lifeperiods.period_end
               AND sessions_plus.hyenas= ','||lifeperiods.ego||','
        GROUP BY lifeperiods.ego, lifeperiods.period;"""

        update_session_count_sql = """UPDATE lifeperiods SET sessions_count =
         (SELECT session_counts.session_count FROM session_counts
         WHERE   lifeperiods.ego    = session_counts.ego
             AND lifeperiods.period = session_counts.period);"""
        update_alone_session_count_sql = """UPDATE lifeperiods SET sessions_alone =
          (SELECT alone_session_counts.alone_session_count FROM alone_session_counts
          WHERE   lifeperiods.ego    = alone_session_counts.ego
              AND lifeperiods.period = alone_session_counts.period);"""

        self.db.execddl("""DROP TABLE IF EXISTS session_counts;""")
        self.db.execddl(temp_session_count_sql)

        self.db.execddl("""DROP TABLE IF EXISTS alone_session_counts;""")
        self.db.execddl(update_session_count_sql)

        self.db.execddl(temp_alone_session_count_sql)
        self.db.execddl(update_alone_session_count_sql)

        #### PREYLEVELS
        # fill in preylevels column nominal value +1/-1 for High/Low
        # collect each month of study preydensity that a period covers
        # calculate average total prey for all overlapping census periods
        # with average prey and

        # this builds a table that averages deviations (sd) from mean of prey density
        # by comparing the dates of the period with the start of the month and end of months 
        # have to build the 'date' of the preydensity table which only has year and month
        # and have to make sure the month has a leading zero (or data function returns null)
        # if average deviation is > 0, then prey density is greater than project average
        temp_preylevels_sql = """CREATE TEMP TABLE preylevels AS SELECT
               lifeperiods.ego AS ego,
               lifeperiods.period AS period,
               CASE WHEN avg(preydensity.numofsd)  >= 0
                    THEN 1
                    ELSE -1
                    END AS prey_level
            FROM preydensity, lifeperiods
            WHERE 
             date(preydensity.year || "-" || printf("%02d", preydensity.month) || '-01')  
                 >= date(lifeperiods.period_start) 
             AND
             date(preydensity.year || "-" || printf("%02d", preydensity.month) || '-01', '+1 month', '-1 day')  
                 <= date( lifeperiods.period_end)
             AND preydensity.clan = 'talek'
            GROUP BY 
               ego,period;"""

        update_preylevels_sql = """UPDATE lifeperiods SET prey_level =
         (SELECT prey_level FROM preylevels
           WHERE lifeperiods.ego    = preylevels.ego
             AND lifeperiods.period = preylevels.period);"""

        # run the above sql to add prey level value to periods table
        self.db.execddl("""DROP TABLE IF EXISTS preylevels;""")
        self.db.execddl(temp_preylevels_sql)
        self.db.execddl(update_preylevels_sql)


        # we've executed all the sql we need, but this method still expects some sql to 
        # execut, so pass dummysql to table builder for bonus sql
        # this doesn't affecdt anything.  COULD have used any of the sql above, but this is 
        # cleaner and allows for more code 
        dummysql = """select 1"""        
        return (dummysql)


class SessionsPlusMaker(TableMaker):
    """create  SessionsPlus table, based on sessions but filtered"""

    def __init__(self, db, max_session_date):
        TableMaker.__init__(self, db)
        # super(SessionsPlusMaker,self).__init__(db)
        self.tablename = 'sessions_plus'
        self.max_session_date = max_session_date

    def make_table_sql(self):
        sql = """CREATE TABLE sessions_plus AS
        SELECT sessions.session as session,
                location, sessiondate, start, stop,
                unidhyenas, other,
                sessions.tracked as tracked,
                seen, pickup,
                ("," || group_concat(hyenaspersession.hyena) || ",")
                    AS hyenas,
                count(hyenaspersession.hyena)
                    as identified_hyena_count,
                (strftime('%s',sessions.stop ) - strftime('%s',sessions.start) ) /60
                    as sessionminutes,  -- eg. DURATION
                max(hyenaspersession.fas)
                    as fas,
                "talek"
                    as clan
        FROM
            sessions INNER JOIN hyenaspersession ON sessions.SESSION = hyenaspersession.session
        -- exclude sessions that cross midnight boundary and give negative durations
        WHERE (strftime('%s',sessions.stop ) - strftime('%%s',sessions.start) ) > 0
           AND (julianday(sessions.sessiondate) <= julianday('{0}') )
        GROUP BY sessions.session
        ORDER BY sessions.sessiondate, sessions.START;""".format(self.max_session_date)

        return (sql)

    def index_sql_array(self):
        return (
            ["CREATE UNIQUE INDEX pk_sessions_plus         ON sessions_plus (session);",
             "CREATE        INDEX idx_sessions_plus_date   ON sessions_plus (sessiondate);",
             "CREATE        INDEX idx_sessions_plus_hyenas ON sessions_plus (hyenas);"])


#####################
# some example periods in format require by the class above
# TODO: move to individual builder


def socialdev_sixmonth_periods():
    periods = [
        {'seq': 1, 'name': '6to12', 'desc': '6 months old to 1 year old',
         'start_expr': "date(hyenas.birthdate, '+6 months')",
         'end_expr': "date(hyenas.birthdate, '+1 year')",
         'where': None},
        {'seq': 2, 'name': '12to18', 'desc': '1 year old to 18 months',
         'start_expr': "date(hyenas.birthdate, '+1 year')",
         'end_expr': "date(hyenas.birthdate, '+18 months')",
         'where': None},
        {'seq': 3, 'name': '18to24', 'desc': '18 months old to 2 yrs',
         'start_expr': "date(hyenas.birthdate, '+18 months')",
         'end_expr': "date(hyenas.birthdate, '+24 months')",
         'where': None},
        {'seq': 4, 'name': '24to30', 'desc': '2 yrs old to 2.5 years old',
         'start_expr': "date(hyenas.birthdate, '+24 months')",
         'end_expr': "date(hyenas.birthdate, '+30 months')",
         'where': None}
    ]
    return (periods)


## note; for now, this is copied here to ensure there is a duplicate
# but it's used in maledisperal_snabuilder.py
def malestudy_periods():
    """returns a array holding the periods for male dispersal study"""
    periods = [
        {'seq': "1",
         'name': "12mBD",
         'desc': '12th month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-360 days')",
         'end_expr': "date(hyenas.disappeared, '-331 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
        {'seq': '2',
         'name': '11mBD',
         'desc': '11th month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-330 days')",
         'end_expr': "date(hyenas.disappeared, '-301 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
        {'seq': '3',
         'name': '10mBD',
         'desc': '10th month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-300 days')",
         'end_expr': "date(hyenas.disappeared, '-271 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
        {'seq': '4',
         'name': '9mBD',
         'desc': '9th month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-270 days')",
         'end_expr': "date(hyenas.disappeared, '-241 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
        {'seq': '5',
         'name': '8mBD',
         'desc': '8th month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-240 days')",
         'end_expr': "date(hyenas.disappeared, '-211 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
        {'seq': '6',
         'name': '7mBD',
         'desc': '7th month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-210 days')",
         'end_expr': "date(hyenas.disappeared, '-181 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
        {'seq': '7',
         'name': '6mBD',
         'desc': '6th month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-180 days')",
         'end_expr': "date(hyenas.disappeared, '-151 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
        {'seq': '8',
         'name': '5mBD',
         'desc': '5th month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-150 days')",
         'end_expr': "date(hyenas.disappeared, '-121 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
        {'seq': '9',
         'name': '4mBD',
         'desc': '4th month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-120 days')",
         'end_expr': "date(hyenas.disappeared, '-91 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
        {'seq': '10',
         'name': '3mBD',
         'desc': '3rd month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-90 days')",
         'end_expr': "date(hyenas.disappeared, '-61 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
        {'seq': '11',
         'name': '2mBD',
         'desc': '2nd month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-60 days')",
         'end_expr': "date(hyenas.disappeared, '-31 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
        {'seq': '12',
         'name': '1mBD',
         'desc': '1st month before dispersal',
         'start_expr': "date(hyenas.disappeared, '-30 days')",
         'end_expr': "date(hyenas.disappeared, '-1 days')",
         'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}
    ]

    return periods
