#!/usr/bin/env python
"""method to build and run sna db builder with params specific to this study"""

import os
import argparse
from snabuilder import SNABuilder
# imports all tablebuilders

import builder_utils


# -- same as milestones study but the egos are mixed sex twin litters
# -- make duration of period 2 & 3 the same num days as period 1
# -- previous this period was from dengrad to weaned,
# -- date(hyenas.weaned, '-1 days') as period_end

def build(dbfile,this_max_session_date = "2013-09-30", verbose=True):
    """ builds the db for this study"""

    periods = [{'seq': "1",
                'name': 'cd',
                'desc': 'dfs to dengrad',
                'start_expr': "hyenas.firstseen",
                'end_expr': "date(hyenas.dengrad, '-1 day')",
                'where': None},
               {'seq': "2",
                'name': "postgrad",
                'desc': 'time after den graduation; duration equal time as period 1',
                'start_expr': "hyenas.dengrad",
                'end_expr': "date(hyenas.dengrad, '+' ||(julianday(hyenas.dengrad)-JulianDay(hyenas.firstseen)-1)|| ' days')",
                'where': None},
               {'seq': "3",
                'name': "adult",
                'desc': 'after 2 cal yrs of age; duration equal time as period 1',
                'start_expr': "date(hyenas.birthdate, '+2 years')",
                'end_expr': "date(hyenas.birthdate, '+' ||(365*2+(julianday(hyenas.dengrad)- JulianDay(hyenas.firstseen)-1))  ||' days')",
                'where': None}
               ]
    
    clan = 'talek'
    clanlist_seen_threshold = 10
    # provide sql for how hyenas will be seleted for this study
    select_hyenas_sql = """select hyenas.mom, hyenas.birthdate, hyenas.hyena as hyena, hyenas.sex, hyenas.dengrad, twinlist.hyena as twin, twinlist.sex as twinsex, twinlist.birthdate as birthdatecheck 
from hyenas inner join hyenas as twinlist on ( hyenas.mom = twinlist.mom and hyenas.birthdate = twinlist.birthdate) 
where hyenas.hyena <> twinlist.hyena and hyenas.sex <> twinlist.sex 
and hyenas.sex <> 'u' and twinlist.sex <> 'u' 
and hyenas.birthdate >= '1989-01-01' 
and hyenas.clan='{0}' 
and hyenas.dengrad is not null and twinlist.dengrad is not null 
and (
    julianday(hyenas.disappeared) >= julianday(date(hyenas.birthdate, "+24 months"))
    or 
    hyenas.disappeared is null)
and (julianday(twinlist.disappeared) >= julianday(date(twinlist.birthdate, "+24 months"))
    or
    twinlist.disappeared is null)
    and julianday(hyenas.firstseen) < julianday('{1}')
order by hyenas.mom, hyenas.birthdate, hyenas.hyena 
;
            """.format(clan, this_max_session_date )
               
    # create a base SNABuilder object with this dbfile and periods above 
    # workingdbfile, periods, max_session_date, select_hyenas_sql = None, clanlist_seen_threshold 
    dbbuilder = SNABuilder(dbfile, periods, this_max_session_date, select_hyenas_sql, clanlist_seen_threshold)

    # customize remaing SNABuilder options
    dbbuilder.clan = clan
    


    print dbbuilder.select_hyenas_sql
    
    # run it! 
    result = dbbuilder.build_db()
    
    print(result)
    
    if verbose:
        dbbuilder.print_result()

def main():
    """ called from command line; send a valid file name for a db, one that has the required tables and no other SNA tables"""
    parser = argparse.ArgumentParser()

    parser.add_argument('dbfile',
                        help='initial database with fisibase tables (required)',
                        type=builder_utils.valid_file )

    parser.add_argument('--verbose', '-v', required=False, help='print progress on command line',
                        action="store_true")
    myargs = parser.parse_args()
    dbfile = myargs.dbfile
    
    build(dbfile,this_max_session_date = "2013-09-30", verbose=myargs.verbose)
    

if __name__ == '__main__':
    main()