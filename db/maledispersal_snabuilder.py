#!/usr/bin/env python
"""method to build and run sna db builder with params specific to this study"""

import os
import argparse
from snabuilder import SNABuilder
# imports all tablebuilders
import builder_utils




periods = [ {'seq': "1",
'name': "12mBD",
'desc': '12th month before dispersal',
'start_expr': "date(hyenas.disappeared, '-360 days')",
'end_expr': "date(hyenas.disappeared, '-331 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
{'seq': '2',
'name': '11mBD',
'desc': '11th month before dispersal',
'start_expr': "date(hyenas.disappeared, '-330 days')",
'end_expr': "date(hyenas.disappeared, '-301 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
{'seq': '3',
'name': '10mBD',
'desc': '10th month before dispersal',
'start_expr': "date(hyenas.disappeared, '-300 days')",
'end_expr': "date(hyenas.disappeared, '-271 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
{'seq': '4',
'name': '9mBD',
'desc': '9th month before dispersal',
'start_expr': "date(hyenas.disappeared, '-270 days')",
'end_expr': "date(hyenas.disappeared, '-241 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
{'seq': '5',
'name': '8mBD',
'desc': '8th month before dispersal',
'start_expr': "date(hyenas.disappeared, '-240 days')",
'end_expr': "date(hyenas.disappeared, '-211 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
{'seq': '6',
'name': '7mBD',
'desc': '7th month before dispersal',
'start_expr': "date(hyenas.disappeared, '-210 days')",
'end_expr': "date(hyenas.disappeared, '-181 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
{'seq': '7',
'name': '6mBD',
'desc': '6th month before dispersal',
'start_expr': "date(hyenas.disappeared, '-180 days')",
'end_expr': "date(hyenas.disappeared, '-151 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
{'seq': '8',
'name': '5mBD',
'desc': '5th month before dispersal',
'start_expr': "date(hyenas.disappeared, '-150 days')",
'end_expr': "date(hyenas.disappeared, '-121 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
{'seq': '9',
'name': '4mBD',
'desc': '4th month before dispersal',
'start_expr': "date(hyenas.disappeared, '-120 days')",
'end_expr': "date(hyenas.disappeared, '-91 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
{'seq': '10',
'name': '3mBD',
'desc': '3rd month before dispersal',
'start_expr': "date(hyenas.disappeared, '-90 days')",
'end_expr': "date(hyenas.disappeared, '-61 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
{'seq': '11',
'name': '2mBD',
'desc': '2nd month before dispersal',
'start_expr': "date(hyenas.disappeared, '-60 days')",
'end_expr': "date(hyenas.disappeared, '-31 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"},
{'seq': '12',
'name': '1mBD',
'desc': '1st month before dispersal',
'start_expr': "date(hyenas.disappeared, '-30 days')",
'end_expr': "date(hyenas.disappeared, '-1 days')",
'where': "hyenas.sex='m'  and hyenas.fate='d' and hyenas.disappeared is not null"}]




""" send a valid file name for a db, one that has the required tables and no other SNA tables"""
parser = argparse.ArgumentParser()

parser.add_argument('dbfile',
                    help='initial database with fisibase tables (required)',
                    type=builder_utils.valid_file )

parser.add_argument('--verbose', '-v', required=False, help='print progress on command line',
                    action="store_true")
myargs = parser.parse_args()
dbfile = myargs.dbfile

if myargs.verbose:
     print('building sna tables in {0}'.format(dbfile))

# options
clan = 'talek'
max_session_date = "2013-09-30"  # current db as of
clanlist_seen_threshold = 10
# provide sql for how hyenas will be seleted for this study
select_hyenas_sql = """SELECT hyenas.hyena as hyena
        FROM hyenas
        WHERE   hyenas.sex       = 'm'
            and hyenas.birthdate is not null
            and hyenas.mom       is not null
            and hyenas.firstseen is not null
            and hyenas.dengrad   is not null
            and julianday(hyenas.firstseen) < julianday(date(hyenas.birthdate, "+90 days"))
            and strftime('%Y', hyenas.firstseen) >= '1989' and hyenas.clan='{0}'
            and julianday(hyenas.firstseen) < julianday('{1}')
        order by  hyenas.birthdate;
        """.format(clan, max_session_date)

dbbuilder = SNABuilder(dbfile, periods, max_session_date, select_hyenas_sql, clanlist_seen_threshold,  myargs.verbose)

xresult = dbbuilder.build_db()
print(xresult)
if myargs.verbose:
    dbbuilder.print_result()
