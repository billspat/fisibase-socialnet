"""Utilities functions called by db builder tablemaker objects """

# builder_utils.py
# methods used by multiple sql builder routines

import os
import shutil
import argparse
from sqlitedb import SqliteDB   #, TableMaker



def valid_file(filepath):
    """used to test if a file is valid, used by programs with command line arguments"""
    if not os.path.exists(filepath):
        msg = "can't find file %s" % filepath
        raise argparse.ArgumentTypeError(msg)
    return filepath

def open_a_copy_db(startdbfile, copydbfile):
    """given a SQLite DB file; copy the file and open the db"""
    # try...
    # erases workingdbfile... confirm if present?
    # TODO test if valid db file and raise RuntimeError("unable to open db")
    shutil.copyfile(startdbfile, copydbfile)
    db = SqliteDB(copydbfile)
    # TODO make sure all required tables are present in
    return (db)


def behavior_index_ddl(tablename):
    """SQL for standard indexing on edge list tables like this sql = behavior_index_ddl(self.tablename) """
    return("CREATE UNIQUE INDEX '{0}-edge' ON {0} (ego, period, H1, H2);".format(tablename))


## this is to  be called by the edgelist tables that need to be normalized.

def normalize_edge_weights_sql(table_name, weight_field_name="weight"):
    """calc all weights to be weight/ai for behavior-based edge lists"""
    # assumes current edge weights in tables are raw counts.
    # TO DO: create raw_weight column to hold raw counts/averages

    # the ordering of names in AI table may not be the same as in behavior tables
    # so need to check both h1 =h1... or h2 = h1.  Probably a better way to do this
    # t1.H1||t1.H2 = t2.H1||t2.H2 or t1.H2,t1.H1) = concat(t2.H1,t2.H2)
    sqltemplate = """UPDATE {0}
            set weight = (
                select round({0}.weight/lifeperiods_ais_el.{1},4)
                from   lifeperiods_ais_el
                where    {0}.ego = lifeperiods_ais_el.ego
                and      {0}.period = lifeperiods_ais_el.period
                and (  ( {0}.H1 = lifeperiods_ais_el.H1 and
                         {0}.H2 = lifeperiods_ais_el.H2 )
                    or ( {0}.H2 = lifeperiods_ais_el.H1 and
                         {0}.H1 = lifeperiods_ais_el.H2 )
                    )
                );"""

    sql = sqltemplate.format(table_name, weight_field_name)
    ## temporary debugging...
    print(sql)
    return(sql)
