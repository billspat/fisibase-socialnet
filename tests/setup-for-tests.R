source("egonet.R")
hyena = "sx"
period_name ="adult"
  
behavior.df = dbTable("lifeperiods_aggressions_el")
edge.df = onePeriod(behavior.df,hyena,period_name)
net_clan  = one_graph(behavior.df, hyena, period_name,directedgraph=TRUE)
net_ego = get.ego.net(net_clan, hyena)
