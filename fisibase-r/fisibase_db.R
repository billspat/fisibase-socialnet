library(DBI)
library(RSQLite)

# this code sets a connection and database file variable in the global scope
# if they are not set already.  Set these variable in your main program
# and you won't have to send a database 'connection' parameter to the db functions

getConnection <- function(filepath=DEFAULT_DB){
  
  if(!file.exists(filepath)) return(FALSE)

  CURRENT_CONN <<- dbConnect(SQLite(), filepath)
  
  # check to make sure there are tables in the db
  if( length(dbtables(CURRENT_CONN)) > 0 ){
    return(CURRENT_CONN )
  } 
  else {
    closeConnection(CURRENT_CONN)
    return(NULL)
  }

}

checkConnection <- function(conn=CURRENT_CONN){
  return ((class(conn) == "SQLiteConnection" && !is.null(conn)) )
}

closeConnection <- function(conn=CURRENT_CONN){
  if (checkConnection()) {
    dbDisconnect(conn)
  }
}


dbmakeview <- function(sql,viewname,conn = CURRENT_CONN) {
  # create a view, dropping if necessary
  # IMPORTANT NOTE for SQLITE.  if the database is overwritten or recreated,
  # any views created here are lost and must be recreated. 
  
  # TO DO : make up random viewname if NA is passed in
  dropview = sprintf("DROP VIEW IF EXISTS %s", viewname)
  makeview = sprintf("CREATE VIEW %s AS %s",viewname,sql)

  # drop any view with the same name
  result = dbSendQuery(conn, dropview)
  if(!dbHasCompleted(result)) { return(FALSE) }
  
  # make the view
  result = dbSendQuery(conn,makeview)
  if(!dbHasCompleted(result)) { return(FALSE) }
  
  # TO DO: TRY VIEW TO SEE IF IT WORKS...
  # and make it optional if trying to save time...
  
  #return viewname here, for when the feature of random view names is in place
  return(viewname) 
  
}

dbQuery <- function(statement, conn = CURRENT_CONN){
  # WITH CURRENT VERSION OF RSQLite, dbIsValid always returns True, 
  # this this function is pretty useless
  
  if( dbIsValid(conn)) {
    return(dbGetQuery(conn, statement))
  }
  else { return(FALSE) }
}

#alias with capital Q for when I forget it's all lowercase
dbquery <- function(statement, conn = CURRENT_CONN) {
  return(dbQuery(statement,conn))
}

# convenience - get all rows from one table into database
dbTable <- function(table, conn = CURRENT_CONN){
    # TO DO : MAKE SURE TABLE EXISTS
    sql = sprintf("SELECT * FROM %s", table)
    return(dbQuery(sql))
}

# the above is a simplistic version of what should be 
# result = dbSendQuery
# check if results or error
# fetch all results to df is 
# dbHasCompleted(result)
# OR send data in batches on different thread



dbtables <- function(conn=CURRENT_CONN) {
    if( dbIsValid(conn)) {
        return(dbListTables(conn))
    }
    else {
        return(FALSE)
    }
}


if(!exists("DEFAULT_DB")){
    if( file.exists("db/fisibase.sqlite3"))
        DEFAULT_DB <- "db/fisibase.sqlite3"
}

if(!exists("CURRENT_CONN")) {
    # check if default default db is set
    CURRENT_CONN <- getConnection()
}

# #######################
# UTILITY FUNCTIONS 
#  move these to another file eventually

validDate <- function(datechar) {
  dformat = "%Y-%m-%d"
  result = TRUE
  d <- try( as.Date( datechar, format= dformat ),silent=TRUE )
  if( class( d ) == "try-error" || is.na( d ) ) result = FALSE
  return(result)
  
}

