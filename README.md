Hyena Social Networking Analysis
===

![example egonets](doc/SNA_egonetfigure_for_docs.png "Sample from Figure 1, 'Ontogenetic change in determinants of social network position in the spotted hyena.' ")

Overview
---

This is Dr. Julie Turner's PhD project for the Mara Hyena Project](https://www.holekamplab.org/) for Social Networking Analysis (SNA) of hyenas by developmental milestones

  * top directories and some subfolders contain Julie's Analysis R code
  * [Database and python database generation code is in db folder](/db)
  * [R libraries to work with fisibase.sqlite in fisibase-r](/fisibase-r)
  * to re-build starting database from current MS Access version, see  https://gitlab.msu.edu/billspat/fisibase-sqlite  
  * test and tests are for confirming results and short test-runs of database pipeline

Techniques
---

this uses the fisibase.sqlite from https://gitlab.msu.edu/billspat/fisibase-sqlite and adds tables for edgelists for network analsys using R [statnet](https://statnet.csde.washington.edu/) and sometimes igraph R package (which was used by a previous version of this code )


Publications
---

2019. JW Turner, PS Bills, KE Holekamp. Ontogenetic change in determinants of social network position in the spotted hyena.  Behavioral Ecology and Sociobiology. 2019 72:10. https://doi.org/10.1007/s00265-017-2426-x.  


2020. JW Turner, PS Bills, KE Holekamp. Early life relationships matter: Social position during early life affects fitness among female spotted hyenas.  Proceedings of the Royal Society B (Biology).  2020.  In press.
