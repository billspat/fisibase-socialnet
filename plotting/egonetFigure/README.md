# Plotting EgoNetworks

*tl;dr The resulting figure is in egonets.html*

*Open that in Firefox to see the figure.   I printed that web page to PDF and then convert to EPS, crop, etc using Inkscape.  *

When the file is first opened, the D3 javascript 

egonets.html uses D3 javascript, based on http://bl.ocks.org/mbostock/1153292 ,  to render the graphs from CSV data generated in R and exported.   The file egonets.html includes the Javascript code.  The page is formatted specifically for a figure in Julie Turner's paper

When the page is first loaded, the Javascript works to create an optimum layout of the network nodes.   Then I manually moved the nodes to make each network more readdable, and printed to PDF.   Ultimately it would be good to find a way to save the coordinates of the nodes onces moved so it can be regenerated exactly the same as it appears in the paper.  

I added code the 'fix' the nodes when they were dragged, so they would stay put, different from the D3 linked above. 

This page has Javascript on it, and some browsers won't run the javascript from your computer (for security).  If you open this file with Firefox the javascript will run and you can see the figures.  If you use a different browser (Chrome, Safari), you may have to either put the file on a website, or run a local mini-webserver in this folder and point your browser to that.    On MacOS, you can use the command line (terminal) and somethign like 

```
cd <this folder>
php -S 127.0.0.1:8080
```

Then open the web browser to folder in which is was started: 

```127.0.0.1:8080/egonets.html```

To create the CSV files in this folder that egonets.html loads, I used R code to convert network data structure to CSV, using on the network and intergraph librariesso that each CSV was an edge list with columns source, target and value.  Value is not used in this figure but could be used to change the look of the edges (links)

Here is example R code to do that, but see the file "createD3CSVs.R"



        load("egos2graph_forPat_2017-08-29.RData") 
        svgFolder = "." 
        for (network in names(himidlo2graph) ) { 
          n1 <- as.network(himidlo2graph[[network]]) 
          n1.df = intergraph::asDF(n1) 
          n1.df.d3 <- data.frame(source=n1.df$vertexes$vertex.names[n1.df$edges$V1],target=n1.df$vertexes$vertex.names[n1.df$edges$V2],value=rep(1,length(n1.df$edges$V2))) 
          csvfilename = paste0(svgFolder,"/",network,".csv") 
          print(csvfilename) 
          write.csv(n1.df.d3,file=csvfilename,quote=FALSE,row.names = FALSE) 
         
        } 

